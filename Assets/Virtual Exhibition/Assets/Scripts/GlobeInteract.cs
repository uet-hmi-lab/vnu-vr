﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobeInteract : MonoBehaviour
{
    public GameObject GlobeControll;
    private int GlobeControllIni = 0;
    public GameObject MainCamera;
    public GameObject ZoomCamera;
    void Start()
    {
        GlobeControll.SetActive(false);
        MainCamera.SetActive(true);
        ZoomCamera.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GlobeControllIni == 1)
        {
            GlobeControll.SetActive(true);
            MainCamera.SetActive(true);
            ZoomCamera.SetActive(false);
        }
        else if (GlobeControllIni == 0)
        {
            GlobeControll.SetActive(false);
            MainCamera.SetActive(true);
            ZoomCamera.SetActive(false);
        }
        else if (GlobeControllIni == 2)
        {
            GlobeControll.SetActive(false);
            MainCamera.SetActive(false);
            ZoomCamera.SetActive(true);
        }
    }
    void OnMouseDown()
    {
        if (GlobeControllIni == 0)
        {
            GlobeControllIni = 1;
        }
    }
    public void OpenGlobe()
    {
        if (GlobeControllIni == 1)
        {
            GlobeControllIni = 2;
        }
    }
    public void NotOpenGlobe()
    {
        if (GlobeControllIni == 1 || GlobeControllIni == 2)
        {
            GlobeControllIni = 0;
        }
    }
}
