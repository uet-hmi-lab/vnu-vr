﻿using UnityEngine.Video;
using UnityEngine;

public class VideoController : MonoBehaviour
{
    private VideoPlayer video;
    public VideoPlayer video1;
    public VideoPlayer video2;
    void Start()
    {
        video1.enabled = false;
        video2.enabled = false;
        video = video1;
    }
    void Update()
    {
        if (ChooseVideo.VideoOrder == 1)
        {
            video1.enabled = true;
            video2.enabled = false;
            video = video1;
        }
        else if (ChooseVideo.VideoOrder == 2)
        {
            video1.enabled = false;
            video2.enabled = true;
            video = video2;
        }

    }
    public void PlayAndPause()
    {
        if (video.isPlaying != true)
        {
            video.Play();
        }
        else
        {
            video.Pause();
        }
    }
}
