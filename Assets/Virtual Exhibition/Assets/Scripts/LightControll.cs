﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightControll : MonoBehaviour
{
    public GameObject Light;
    public GameObject AmbLight;
    // Start is called before the first frame update
    void Start()
    {
        Light.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        Light.SetActive(true);
        AmbLight.SetActive(false);
    }
    void OnTriggerExit(Collider other)
    {
        Light.SetActive(false);
        AmbLight.SetActive(true);
    }
}
