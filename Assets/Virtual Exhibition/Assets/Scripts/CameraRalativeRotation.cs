﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRalativeRotation : MonoBehaviour
{
    public GameObject CameraRelative;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(CameraRelative.transform.eulerAngles.x, CameraRelative.transform.eulerAngles.y, 0);
    }
}
