﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrollFOV : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis ("Mouse ScrollWheel") > 0)
        {
            //while (GetComponent<Camera > ().fieldOfView > 10)
            GetComponent<Camera>().fieldOfView--;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            //while (GetComponent<Camera > ().fieldOfView < 80)
            GetComponent<Camera>().fieldOfView++;
        }
    }
}
