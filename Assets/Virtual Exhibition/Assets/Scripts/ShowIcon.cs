﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowIcon : MonoBehaviour
{
    public GameObject icon;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnMouseEnter()
    {
        icon.SetActive(true);
    }
    void OnMouseExit()
    {
        icon.SetActive(false);
    }
}
