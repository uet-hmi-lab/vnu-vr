﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public class FPSMoveByClick : MonoBehaviour
{
    public enum RotationAxis
    {
        MouseX = 1,
        MouseY = 2
    }
    public RotationAxis axes = RotationAxis.MouseX;
    public float sensHorizontal = 2.0f;
    public float sensVertical = 2.0f;
    public float _rotationX = 0;
    public float speed = 6.0f;
    public float gravity = -9.8f;
    public new GameObject animation;
    public Camera eye;
    private bool isClick;
    private long timeMouseDown;
    private NavMeshAgent agent;
    private CharacterController _charCont;
    void Start()
    {
        isClick = false;
        agent = GetComponent<NavMeshAgent>();
        _charCont = GetComponent<CharacterController>();
        animation.GetComponent<NavMeshAgent>().enabled = true;
    }



    // Update is called once per frame
    void Update()
    {
        RaycastHit hit_mouse;
        NavMeshAgent agent_animation;
        agent_animation = animation.GetComponent<NavMeshAgent>();

        Ray ray_mouse = eye.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray_mouse, out hit_mouse, Mathf.Infinity))
        {
            agent_animation.SetDestination(hit_mouse.point);
        }

        if (Input.GetMouseButtonDown(0))
        {
            timeMouseDown = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            long timeMouseUp = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            long diffTime = timeMouseUp - timeMouseDown;
            if (diffTime < 300)
            {
                isClick = true;
            }
        }
        // Move by mouse click
        if (isClick)
        {
            isClick = false;
            RaycastHit hit;
            Ray ray = eye.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                agent.SetDestination(hit.point);
            }
        }
        // Rotate by hold mouse
        if (Input.GetMouseButton(0))
        {
            if (axes == RotationAxis.MouseX)
            {
                transform.Rotate(0, -Input.GetAxis("Mouse X") * sensHorizontal, 0);
            }
            else if (axes == RotationAxis.MouseY)
            {
                _rotationX -= Input.GetAxis("Mouse Y") * sensVertical;
                float rotationY = transform.localEulerAngles.y;
                transform.localEulerAngles = new Vector3(-_rotationX, rotationY, 0);
            }
        }
    }
}
