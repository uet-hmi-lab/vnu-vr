﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSMoveButton : MonoBehaviour
{
    public CharacterController player;
    public enum RotationAxis
    {
        MouseX = 1,
        MouseY = 2
    }
    public float sensHorizontal = 2.0f;
    public float sensVertical = 2.0f;
    public float _rotationX = 0;
    public float speed = 0.1f;
    public RotationAxis axes = RotationAxis.MouseX;

    void Update()
    {
        float xDirection = Input.GetAxis("Horizontal");
        float zDirection = Input.GetAxis("Vertical");

        Vector3 moveDirection = transform.right * xDirection + transform.forward * zDirection;
        player.Move(moveDirection * speed * Time.deltaTime);
        if (Input.GetMouseButton(0))
        {
            if (axes == RotationAxis.MouseX)
            {
                transform.Rotate(0, -Input.GetAxis("Mouse X") * sensHorizontal, 0);
            }
            else if (axes == RotationAxis.MouseY)
            {
                _rotationX -= Input.GetAxis("Mouse Y") * sensVertical;
                float rotationY = transform.localEulerAngles.y;
                transform.localEulerAngles = new Vector3(-_rotationX, rotationY, 0);
            }
        }
    }
}