﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRenderQueue : MonoBehaviour
{
    public int RenderQueueNum;

    void Start()
    {
        GetComponent<MeshRenderer>().material.renderQueue = RenderQueueNum;
    }
}
