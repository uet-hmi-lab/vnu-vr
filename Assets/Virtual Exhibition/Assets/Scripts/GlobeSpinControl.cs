﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobeSpinControl : MonoBehaviour
{
    public GameObject Globe;
    private int RotateTo = 0;
    [SerializeField] [Range(0f, 5f)] float lerpTime;
    [SerializeField] Vector3[] myAngles;
    
    int angleIndex;
    int len;

    float t = 0f;

    void Start()
    {
        len = myAngles.Length;
    }
    public void Update()
    {
        if (RotateTo == 1)
        {
            Globe.transform.rotation = Quaternion.Slerp(Globe.transform.rotation,
            Quaternion.Euler(myAngles[angleIndex]), lerpTime * Time.deltaTime);

            t = Mathf.Lerp(t, 1f, lerpTime * Time.deltaTime);
            if (t > .9f)
            {
                t = 0f;
                angleIndex = Random.Range(0, len - 1);
            }
        }        
    }
    void OnMouseDown()
    {
        if (RotateTo == 0)
        {
            RotateTo = 1;
        }
    }
}
