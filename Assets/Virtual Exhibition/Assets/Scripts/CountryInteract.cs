﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryInteract : MonoBehaviour
{
    public GameObject Country;
    public GameObject Pictrue;
    public Material MattIni;
    public GameObject SoundToCountry;
    public float DissolveNum=0.5f;
    public float speed = 0.05f;
    private int DissolveIni = 0;
    void Start()
    {
        MattIni = Pictrue.GetComponent<Renderer>().material;
        SoundToCountry.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (DissolveIni == 1)
        {
            SoundToCountry.SetActive(true);
            if (DissolveNum > -1.1f)
            {
                DissolveNum -= speed;
            }
        }
        else if (DissolveIni == 0)
        {
            SoundToCountry.SetActive(false);
            if (DissolveNum < 0.6f)
            {
                DissolveNum += speed;
            }
        }
        MattIni.SetFloat("Disolve", DissolveNum);
        Debug.Log(DissolveIni);
    }
    void OnMouseDown()
    {
        if (DissolveIni == 0)
        {
            DissolveIni = 1;
        }
        else if (DissolveIni == 1)
        {
            DissolveIni = 0;
        }
    }
}