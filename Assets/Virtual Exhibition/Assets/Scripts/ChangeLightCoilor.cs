﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLightCoilor : MonoBehaviour
{
    public GameObject LightInDoor;
    private Color startcolor = new Color(0.9f, 0f, 0f, 1f);
    private Color newcolor = new Color(0, 1, 0, 1);
    // Start is called before the first frame update
    void Start()
    {
        LightInDoor.GetComponent<Renderer>().material.color = startcolor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        LightInDoor.GetComponent<Renderer>().material.color = newcolor;
    }
    void OnTriggerExit(Collider other)
    {
        LightInDoor.GetComponent<Renderer>().material.color = startcolor;
    }
}

