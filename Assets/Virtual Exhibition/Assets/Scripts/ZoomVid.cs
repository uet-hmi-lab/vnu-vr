﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public class ZoomVid : MonoBehaviour
{
    public static string ObjectActive = "f";
    public GameObject TheCamera;
    public GameObject controller;
    public VideoPlayer video1;
    public VideoPlayer video2;
    private VideoPlayer ThePlayingVideo;
    public GameObject HMILogo;
    private int flag = 0;

    // Start is called before the first frame update
    void Start()
    {
        video1.enabled = false;
        video2.enabled = false;
        ThePlayingVideo = video1;
    }

    // Update is called once per frame
    void Update()
    {
        if (ChooseVideo.VideoOrder == 1)
        {
            video1.enabled = true;
            video2.enabled = false;
            ThePlayingVideo = video1;
        }
        else if (ChooseVideo.VideoOrder == 2)
        {
            video1.enabled = false;
            video2.enabled = true;
            ThePlayingVideo = video2;
        }
        if (ZoomVid.ObjectActive == "t")
        {
            TheCamera.SetActive(true);
            HMILogo.SetActive(false);
        }
        else if (ZoomVid.ObjectActive == "f")
        {
            TheCamera.SetActive(false);
            HMILogo.SetActive(true);
            controller.GetComponent<Camera>().enabled = true;
        }
    }
    void OnMouseDown()
    {
        ObjectActive = "t";
        controller.GetComponent<Camera>().enabled = true;
        if (flag == 0)
        {
        ChooseVideo.VideoOrder = 1;
        flag++;
        }
    }
    public void ExitZoom()
    {
        ObjectActive = "f";
        ThePlayingVideo.Stop();
    }
}
