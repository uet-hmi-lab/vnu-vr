﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DoorControll : MonoBehaviour
{
    public Animator LeftDoor;
    public Animator RightDoor;
    public AudioSource BGMusic;
    private float FadeSpeed = 0.012f;
    private int BGMini;


    public GameObject player;
    public GameObject checkDoorPoint;
    public float distance;
    public float KcKyvong;

    void Start()
    {
        BGMusic.volume = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (BGMini == 1)
        {
            BGMusic.volume = BGMusic.volume - FadeSpeed;
        }
        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - checkDoorPoint.transform.position.x), Mathf.Abs(player.transform.position.z - checkDoorPoint.transform.position.z));
        if (distance < KcKyvong)
        {
            LeftDoor.SetTrigger("OpenLeftDoor");
            RightDoor.SetTrigger("OpenRightDoor");
        }
        if(distance >= KcKyvong)
        {
            LeftDoor.SetTrigger("CloseLeftDoor");
            RightDoor.SetTrigger("CloseRightDoor");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        //LeftDoor.SetTrigger("OpenLeftDoor");
        //RightDoor.SetTrigger("OpenRightDoor");
        BGMini = 1;
    }
    void OnTriggerExit(Collider other)
    {
        //LeftDoor.SetTrigger("CloseLeftDoor");
        //RightDoor.SetTrigger("CloseRightDoor");
    }


}
