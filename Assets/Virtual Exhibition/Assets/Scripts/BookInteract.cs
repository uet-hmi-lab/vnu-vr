﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookInteract : MonoBehaviour
{
    public GameObject BookControll;
    private int BookControllIni = 0;
    public GameObject MainCamera;
    public GameObject ZoomCamera;
    public GameObject Page1;
    public GameObject Page2;
    public GameObject Holo;
    public Animator PageToFlip;
    private int PageControl = 0;
    void Start()
    {
        BookControll.SetActive(false);
        MainCamera.SetActive(true);
        ZoomCamera.SetActive(false);
        Page1.SetActive(false);
        Page2.SetActive(false);
        Holo.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (BookControllIni == 1)
        {
            BookControll.SetActive(true);
            MainCamera.GetComponent<Camera>().enabled = false;
            ZoomCamera.SetActive(true);
            Holo.SetActive(true);
        }
        else if (BookControllIni == 0)
        {
            BookControll.SetActive(false);
            MainCamera.GetComponent<Camera>().enabled = true;
            ZoomCamera.SetActive(false);
            Holo.SetActive(false);
        }
        if (PageControl == 0)
        {
            Page1.SetActive(false);
            Page2.SetActive(false);
        }
        else if (PageControl == 1)
        {
            Page1.SetActive(true);
            Page2.SetActive(false);
        }
        else if (PageControl == 2)
        {
            Page1.SetActive(false);
            Page2.SetActive(true);
        }
    }
    void OnMouseDown()
    {
        if (BookControllIni == 0)
        {
            BookControllIni = 1;
            PageControl = 1;
        }
        else if (BookControllIni == 1)
        {
            BookControllIni = 0;
        }
    }
    public void NextPage()
    {
        if (PageControl < 2)
        {
            PageControl = PageControl + 1;
        }
        PageToFlip.SetTrigger("Flip");
    }
    public void PrePage()
    {
        if (PageControl > 1)
        {
            PageControl = PageControl - 1;
        }
        PageToFlip.SetTrigger("Flip");
    }
    public void ExitBook()
    {
        if (BookControllIni == 1)
        {
            BookControllIni = 0;
            PageControl = 0;
        }
    }
}
