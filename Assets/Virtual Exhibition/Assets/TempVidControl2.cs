﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class TempVidControl2 : MonoBehaviour
{
    public VideoPlayer Video1;
    public GameObject Object;
    public Material Screen;
    private int VideoPlay = 0;
    void Start()
    {
        Video1.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (VideoPlay == 1)
        {
            Video1.enabled = true;
        }
        if (VideoPlay == 0)
        {
            Video1.enabled = false;
        }
    }
    void OnMouseDown()
    {
        Object.GetComponent<MeshRenderer> ().material = Screen;

        if (VideoPlay == 0)
        {
            VideoPlay = 1;
        }
        else if (VideoPlay == 1)
        {
            VideoPlay = 0;
        }
    }
}
