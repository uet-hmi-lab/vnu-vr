﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeLanguage : MonoBehaviour
{
    public Button langButton;
    private bool langState;
    public GameObject Vie;
    public GameObject Eng;
    // Start is called before the first frame update
    void Start()
    {
        langState = true;
        Vie.SetActive(langState);
        Eng.SetActive(!langState); 
    }

    // Update is called once per frame
    void Update()
    {
        Button btn = langButton.GetComponent<Button>();
        btn.onClick.AddListener(changeLangOnClick);
    }

    void changeLangOnClick()
    {
        langState = !langState;
        Vie.SetActive(langState);
        Eng.SetActive(!langState); 
    }
}
