﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightNode : MonoBehaviour
{
    public GameObject border;
    private Color startcolor;
    private Color newColor;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnMouseEnter()
    {
        startcolor = GetComponent<Renderer>().material.color;
        newColor  = new Color(startcolor.r - 0.2f, startcolor.g - 0.2f, startcolor.b - 0.2f, 0.5f);
        GetComponent<Renderer>().material.color = newColor;
        border.GetComponent<Renderer>().material.color = newColor;
    }
    void OnMouseExit()
    {
        GetComponent<Renderer>().material.color = startcolor;
        border.GetComponent<Renderer>().material.color = startcolor;
    }
}
