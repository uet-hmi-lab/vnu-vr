﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class NodeInformation : MonoBehaviour
{
    [Header("Node's information. Put all you have in here!")]
    public BorderColors BorderColor;
    public bool FlipNode;
    public GameObject[] points;
    public Sprite Picture;
    public string Time;
    [TextArea(5,10)]
    public string VietnameseScript;
    public AudioClip VietnameseAudio;
    [TextArea(5,10)]
    public string EnglishScript;
    public AudioClip EnglishAudio;

    public enum BorderColors{red, green, blue};

    

    [Header("GameObject's settings. Change these carefully, it might break.")]


    [HideInInspector] public GameObject Border;
    [HideInInspector] public TextMeshPro Year;
    [HideInInspector] public GameObject PicturePlane;
    [HideInInspector] public GameObject Frame;
    [HideInInspector] public Image PictureInCanvas;
    [HideInInspector] public TextMeshProUGUI VieCanvas;
    [HideInInspector] public AudioSource VieAudio; 
    [HideInInspector] public TextMeshProUGUI EngCanvas;
    [HideInInspector] public AudioSource EngAudio;
    [HideInInspector] public GameObject childPicture;
    [HideInInspector] public GameObject childYear;

    private Color[] colors;
    private Material mat;

    private LineRenderer lr;
    private float LineWidth;
    

    // Start is called before the first frame update
    void Start()
    {
        //Add colors
        int count = System.Enum.GetValues(typeof(BorderColors)).Length;
        colors = new Color[count];
        colors[(int)BorderColors.red] = new Color(0.960f, 0.278f, 0.278f, 1);
        colors[(int)BorderColors.blue] = new Color(0.278f, 0.392f, 0.960f, 1);
        colors[(int)BorderColors.green] = new Color(0.278f, 0.960f, 0.360f, 1);

        //bind color
        var borderRenderer = Border.GetComponent<Renderer>();
        var frameRenderer = Frame.GetComponent<Renderer>();

        borderRenderer.material.color = colors[(int)BorderColor];
        frameRenderer.material.color = colors[(int)BorderColor];
        mat = borderRenderer.material;

        //bind picutre
        var PictureRenderer = PicturePlane.GetComponent<Renderer>();
        PictureRenderer.material.mainTexture = Picture.texture;

        PictureInCanvas.sprite = Picture;

        //bind script
        VieCanvas.text = VietnameseScript;
        EngCanvas.text = EnglishScript;

        //bind year
        var yearRenderer = Year.GetComponent<TextMeshPro>();
        yearRenderer.text = Time;

        //bind audio
        VieAudio.clip = VietnameseAudio;
        EngAudio.clip = EnglishAudio;

        //line
        lr = GetComponent<LineRenderer>();
        lr.positionCount = (points.Length * 4) + 1;
        lr.material = mat;

        LineWidth = transform.localScale.x * 6 / 11;
        lr.startWidth = LineWidth;
        lr.endWidth = LineWidth;

        lr.SetPosition(0, transform.position);

        Vector3 avg;
        for(int i = 1; i < (points.Length * 4) + 1; i++){
            switch (i % 4)
            {
                case 1: case 3:
                    avg = new Vector3((transform.position.x + points[i/4].transform.position.x)/2, transform.position.y, (transform.position.z + points[i/4].transform.position.z)/2);
                    lr.SetPosition(i, avg);
                    break;
                case 2:
                    lr.SetPosition(i, points[i/4].transform.position);
                    break;
                case 0:
                    lr.SetPosition(i, transform.position);
                    break;

            }
        }

        //flipping model
        if(FlipNode == true){
            Border.transform.Rotate(Vector3.up, 180.0f, Space.Self);
            childPicture.transform.Rotate(Vector3.back, 180.0f, Space.Self);
            childYear.transform.Rotate(Vector3.back, 180.0f, Space.Self);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
