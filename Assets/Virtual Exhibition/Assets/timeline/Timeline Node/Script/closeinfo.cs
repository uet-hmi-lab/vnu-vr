﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class closeinfo : MonoBehaviour
{
    public GameObject Canvas;
    public Button exitButton;
    // Start is called before the first frame update
    void Start()
    {
        Button btn = exitButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void TaskOnClick()
    {    
            Canvas.SetActive(false);
    }
}
