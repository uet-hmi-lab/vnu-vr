﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openinfo : MonoBehaviour
{
    public GameObject Canvas;
    private GameObject[] cv;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        cv = GameObject.FindGameObjectsWithTag("canvas");

        foreach (GameObject c in cv)
        {
            c.SetActive(false);
        }

        Canvas.SetActive(true);
    }


}
