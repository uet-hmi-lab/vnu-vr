﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverPage : MonoBehaviour
{
    public static int PageState = 0;
    public Animator coverAnimator;
    public Animator Page1Animator;
    public Animator Page2Animator;
    public static int CoverIni = 0;
    public static int Page1Ini = 0;
    public static int Page2Ini = 1;
    public void OpenBook()
    {
        StartCoroutine(CoverControlOpen());
    }
    public void CloseBook()
    {
        StartCoroutine(CoverControlClose());
    }

    IEnumerator CoverControlOpen()
    {
        if (PageState == 0)
        {
            coverAnimator.SetTrigger("OpenBook");
            CoverIni = 1;
        }
        else if (PageState == 1)
        {
            Page1Animator.SetTrigger("NextPage1");
            Page1Ini = 1;
            Page2Ini = 0;
        }
        else if (PageState == 2)
        {
            Page2Animator.SetTrigger("NextPage2");
        }
        else if (PageState > 2)
        {
            Debug.Log("No More Page To Next");
        }

        yield return new WaitForSeconds(5.1f);

        if (PageState == 0)
        {
            PageState = 1;
        }
        else if (PageState == 1)
        {
            PageState = 2;
        }
        else if (PageState == 2)
        {
            PageState = 3;
        }
        else if (PageState > 2)
        {
            PageState = 3;
        }

    }

    IEnumerator CoverControlClose()
    {
        if (PageState == 1)
        {
            coverAnimator.SetTrigger("CloseBook");
            CoverIni = 0;
        }
        else if (PageState == 2)
        {
            Page1Animator.SetTrigger("BackPage1");
            Page1Ini = 0;
            Page2Ini = 1;
        }
        else if (PageState == 3)
        {
            Page2Animator.SetTrigger("BackPage2");
        }
        else if (PageState < 1)
        {
            Debug.Log("No More Page To Back");
        }

        yield return new WaitForSeconds(5.1f);

        if (PageState == 1)
        {
            PageState = 0;
        }
        else if (PageState == 2)
        {
            PageState = 1;
        }
        else if (PageState == 3)
        {
            PageState = 2;
        }
        else if (PageState < 1)
        {
            PageState = 0;
        }

    }
    void Update()
    {
    }
}
