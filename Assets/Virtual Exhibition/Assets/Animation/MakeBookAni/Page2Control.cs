﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page2Control : MonoBehaviour
{
    public GameObject Front2;
    void Start()
    {
        Front2.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (CoverPage.PageState == 2)
        {
            if (CoverPage.Page2Ini == 0 && CoverPage.Page1Ini == 1)
            {
                Front2.SetActive(true);
            }
            else
            {
                Front2.SetActive(false);
            }
        }
        else
        {
            Front2.SetActive(false);
        }
    }
}
