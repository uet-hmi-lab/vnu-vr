﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page1Control : MonoBehaviour
{
    public GameObject Front1;
    public GameObject Back1;
    void Start()
    {
        Back1.SetActive(false);
        Front1.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (CoverPage.PageState == 1)
        {
            if (CoverPage.Page1Ini == 0 && CoverPage.CoverIni == 1)
            {
                Back1.SetActive(false);
                Front1.SetActive(true);
            }
            else
            {
                Back1.SetActive(false);
                Front1.SetActive(false);
            }
        }
        else if (CoverPage.PageState == 2)
        {
            if (CoverPage.Page1Ini == 1)
            {
                Back1.SetActive(true);
                Front1.SetActive(false);
            }
            else
            {
                Back1.SetActive(false);
                Front1.SetActive(false);
            }
        }
        else
        {
            Back1.SetActive(false);
            Front1.SetActive(false);
        }
    }
}
