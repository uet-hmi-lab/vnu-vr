﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class QuocKy : MonoBehaviour
{
    public GameObject player;
    public GameObject audiopoint;
    public float distance;
    public float KcKyvong;

    public AudioSource QuocCa;

    bool isTalking = false;
    bool outOfRange = true;

    public GameObject canvas;
    void Start()
    {
        canvas.SetActive(false);
    }
    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            canvas.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - audiopoint.transform.position.x), Mathf.Abs(player.transform.position.z - audiopoint.transform.position.z));
        PressEtoStartCondition();

        if (distance <= KcKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            canvas.SetActive(true);
        }
        else canvas.SetActive(false);

        if (outOfRange == false)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                QuocCa.Play();
                isTalking = true;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                isTalking = false;
                QuocCa.Stop();
            }
        }
        else
        {
            isTalking = false;
            QuocCa.Stop();
        }

    }
}
