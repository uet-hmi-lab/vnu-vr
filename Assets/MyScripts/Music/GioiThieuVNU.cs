﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GioiThieuVNU : MonoBehaviour
{
    public GameObject player;
    public GameObject audiopoint;
    public float distance;
    public float KcKyvong;

    public AudioSource eng;
    public AudioSource viet;

    bool isTalking = false;
    bool outOfRange = true;

    public GameObject canvas;
    void Start()
    {
        canvas.SetActive(false);
    }
    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            canvas.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - audiopoint.transform.position.x), Mathf.Abs(player.transform.position.z - audiopoint.transform.position.z));
        PressEtoStartCondition();

        if (distance <= KcKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            canvas.SetActive(true);
        }
        else canvas.SetActive(false);

        if(outOfRange == false)
        {
            if(Input.GetKeyDown(KeyCode.A))
            {
                eng.Play();
                viet.Stop();
                isTalking = true;
            }
            else if(Input.GetKeyDown(KeyCode.V))
            {
                viet.Play();
                eng.Stop();
                isTalking = true;
            }
            else if(Input.GetKeyDown(KeyCode.S))
            {
                isTalking = false;
                viet.Stop();
                eng.Stop();
            }
        }
        else
        {
            isTalking = false;
            viet.Stop();
            eng.Stop();
        }

        //if (distance <= KcKyvong)
        //{
        //    canvas.SetActive(true);
        //    if (Input.GetKeyDown(KeyCode.A))
        //    {
        //        eng.Play();
        //        viet.Stop();
        //        canvas.SetActive(false);
        //    }
        //    if (Input.GetKeyDown(KeyCode.V))
        //    {
        //        viet.Play();
        //        eng.Stop();
        //        canvas.SetActive(false);
        //    }
        //}
        //else canvas.SetActive(false);

       
        //if(distance > KcKyvong || Input.GetKeyDown(KeyCode.S))
        //{
        //    eng.Stop();
        //    viet.Stop();
        //}
    }
}
