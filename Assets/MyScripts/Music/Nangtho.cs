﻿using UnityEngine;

public class Nangtho : MonoBehaviour
{
    public GameObject player;
    public GameObject audiopoint;
    public float distance;
    public float KcKyvong;
    public AudioSource nangtho;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - audiopoint.transform.position.x), Mathf.Abs(player.transform.position.z - audiopoint.transform.position.z));
        
        if (distance < KcKyvong && Input.GetKeyDown(KeyCode.P))
        {
            nangtho.Play();
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            nangtho.Stop();
        }
    }
}
