﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Video : MonoBehaviour
{
    public GameObject player;
    public GameObject videopoint;
    public VideoPlayer video;
    public float distance;
    public float KcKyvong;

    bool isTalking = false;
    bool outOfRange = true;

    public GameObject canvas;
    void Start()
    {
        canvas.SetActive(false);
    }
    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            canvas.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - videopoint.transform.position.x), Mathf.Abs(player.transform.position.z - videopoint.transform.position.z));
        PressEtoStartCondition();

        if (distance <= KcKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            canvas.SetActive(true);
        }
        else canvas.SetActive(false);

        if (outOfRange == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                video.enabled = true;
                isTalking = true;
            }

            else if (Input.GetKeyDown(KeyCode.S))
            {
                isTalking = false;
                video.enabled = false;
            }
        }
        else
        {
            isTalking = false;
            video.enabled = false;
        }
    }
}
