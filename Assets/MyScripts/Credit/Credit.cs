﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Credit : MonoBehaviour
{
    public GameObject player;
    public GameObject foodpoint;
    public GameObject canvas;
    public float distance;
    public float KcKyvong;

    public GameObject UICanvas;

    bool isTalking = false;
    bool outOfRange = true;

    //public GameObject canvas;

    public float speed = 100f;
    float textPosBegin = -867f;
    float boundaryTextEnd = 1000f;

    RectTransform myGorectTransform;
    [SerializeField]
    TextMeshProUGUI mainText;

    [SerializeField]
    public bool isLooping = false;
    // Start is called before the first frame update
    void Start()
    {
        myGorectTransform = gameObject.GetComponent<RectTransform>();
        UICanvas.SetActive(false);
        //StartCoroutine(AutoScrollText());
        canvas.SetActive(false);
    }
    IEnumerator AutoScrollText()
    {
        while (myGorectTransform.localPosition.y < boundaryTextEnd)
        {
            myGorectTransform.Translate(Vector3.up * speed * Time.deltaTime);
            if (myGorectTransform.localPosition.y > boundaryTextEnd)
            {
                if (isLooping)
                {
                    myGorectTransform.localPosition = Vector3.up * textPosBegin;
                }
                else
                {
                    break;
                }

            }

            yield return null;
        }
    }

    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            canvas.SetActive(false);
            UICanvas.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - foodpoint.transform.position.x), Mathf.Abs(player.transform.position.z - foodpoint.transform.position.z));
        PressEtoStartCondition();

        if (distance <= KcKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            canvas.SetActive(true);
        }
        else
        {
            canvas.SetActive(false);
        }
        if (outOfRange == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                UICanvas.SetActive(true);
                StartCoroutine(AutoScrollText());
                isTalking = true;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                isTalking = false;
                UICanvas.SetActive(false);
                StopCoroutine(AutoScrollText());
                myGorectTransform.localPosition = Vector3.up * textPosBegin;
                StopCoroutine(AutoScrollText());

            }
        }
        else
        {
            isTalking = false;
            UICanvas.SetActive(false);
            StopCoroutine(AutoScrollText());
            myGorectTransform.localPosition = Vector3.up * textPosBegin;
            StopCoroutine(AutoScrollText());

        }
    }

}
