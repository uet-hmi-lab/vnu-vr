﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public NPC npc;
    public GameObject NPCBody;
    public GameObject player;

    bool isTalking = false;

    public float distance;
    float curResponseTracker = 0;
    public float khoangcachKyvong;

    public GameObject dialogueUI;

    public Text npcName;
    public Text npcDialogueBox;
    //public Text playerResponse;

    bool outOfRange;
    public GameObject dialogueGUI;
    bool dialogueUIisActive = false;
    int current = 0;
    //public float letterDelay = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        dialogueUI.SetActive(false);
        dialogueGUI.SetActive(false);

    }
    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            dialogueGUI.SetActive(false);
            dialogueUI.SetActive(false);
        }
    }
    //public IEnumerator TypeDialogue(string line)
    //{
    //    foreach(var letter in line.ToCharArray)
    //    {

    //    }

    //}
    public void OnMouseOver()
    {
        PressEtoStartCondition();
        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - NPCBody.transform.position.x), Mathf.Abs(player.transform.position.z - NPCBody.transform.position.z));//Vector3.Distance(player.transform.position, this.transform.position);

        if (distance <= khoangcachKyvong )
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            dialogueGUI.SetActive(true);
        }
        else dialogueGUI.SetActive(false);

        if (outOfRange == false)
        {
            //dialogueGUI.SetActive(true);

            //if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            //{
            //    curResponseTracker++;
            //    if (curResponseTracker >= npc.playerDialogue.Length - 1)
            //    {
            //        curResponseTracker = npc.playerDialogue.Length - 1;
            //    }
            //}
            //else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            //{
            //    curResponseTracker--;
            //    if (curResponseTracker < 0)
            //    {
            //        curResponseTracker = 0;
            //    }
            //}

            //trigger dialogue
            if (Input.GetKeyDown(KeyCode.E) && isTalking == false)
            {
                dialogueGUI.SetActive(false);
                StartConversation();
            }
            else if (Input.GetKeyDown(KeyCode.E) && isTalking == true)
            {
                dialogueGUI.SetActive(false);
                EndDialogue();
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                npcDialogueBox.text = npc.dialogue[current];
                Debug.Log(current);
                current++;
                if (current > npc.dialogue.Length - 1)
                {
                    current = 0;
                }
                dialogueGUI.SetActive(false);
            }


            //if (curResponseTracker == 0 && npc.playerDialogue.Length >= 0)
            //{
            //    playerResponse.text = npc.playerDialogue[0];
            //    if (Input.GetKeyDown(KeyCode.Return))
            //    {
            //        npcDialogueBox.text = npc.dialogue[1];
            //    }
            //}
            //else if (curResponseTracker == 1 && npc.playerDialogue.Length >= 1)
            //{
            //    playerResponse.text = npc.playerDialogue[1];
            //    if (Input.GetKeyDown(KeyCode.Return))
            //    {
            //        npcDialogueBox.text = npc.dialogue[2];
            //    }
            //}
            //else if (curResponseTracker == 2 && npc.playerDialogue.Length >= 2)
            //{
            //    playerResponse.text = npc.playerDialogue[2];
            //    if (Input.GetKeyDown(KeyCode.Return))
            //    {
            //        npcDialogueBox.text = npc.dialogue[3];
            //    }
            //}

        }
    }

    void StartConversation()
    {
        isTalking = true;
        curResponseTracker = 0;
        dialogueUI.SetActive(true);
        dialogueUIisActive = true;
        npcName.text = npc.name;
        npcDialogueBox.text = npc.dialogue[0];
        dialogueGUI.SetActive(false);
    }

    void EndDialogue()
    {
        isTalking = false;
        dialogueUI.SetActive(false);
        dialogueUIisActive = false;
        dialogueGUI.SetActive(false);
    }

}
