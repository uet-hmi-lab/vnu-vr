﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // khởi động bộ điều khiển chuyển động của Unity
    public CharacterController controller;

    public float speed = 12f;

    // khai báo biến trọng lực
    public float gravity = -9.81f;
    // khai báo biến vận tốc
    Vector3 velocity;



    // khai báo biến kiểm tra đất
    public Transform groundCheck;

    // bán kính của khối cầu kiểm tra muốn tạo ra
    public float groundDistance = 0.4f;

    // khai báo layer mong muốn
    public LayerMask groundMask;

    bool isGrounded;

    public float jumpHeight = 3f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        // khởi tạo hàm kiểm tra khối cầu
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = 0f;
        }


        // lấy ra thông tin theo chiều ngang và chiều dọc
        float x = Input.GetAxis("Horizontal");  // chiều ngang
        float z = Input.GetAxis("Vertical");    // chiều dọc

        // y luôn luôn nằm thẳng đứng nên di chuyển trên bề mặt chỉ xét X và Z
        Vector3 move = transform.right * x + transform.forward * z; // hướng di chuyển là sang ngang (chiều x) và hướng về z

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime; // vận tốc = trọng lực * thời gian

        controller.Move(velocity * Time.deltaTime); // delta y = 1/2 * g * t^2 
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -0.2f * gravity); // do v = căn(2gh)
        }


    }


}
