﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftScrollDoor : MonoBehaviour
{
    public GameObject player;
    public GameObject checkDoorPoint;
    public float distance;
    public float KcKyvong;
    public Vector3 openDes;
    public Vector3 closeDes;
    public GameObject door;

    public float speed;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - checkDoorPoint.transform.position.x), Mathf.Abs(player.transform.position.z - checkDoorPoint.transform.position.z));
        if (distance < KcKyvong)
        {
            door.transform.position = Vector3.Lerp(door.transform.position, openDes, speed * Time.deltaTime); 
        }
        
        if (distance >= KcKyvong)
        {
            door.transform.position = Vector3.Lerp(door.transform.position, closeDes, speed * Time.deltaTime);
        }
    }
}
