﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowLocation : MonoBehaviour
{
    public GameObject playerCheckPoint;

    public GameObject vnuStore;
    public float Distance_vnuStore;
    public GameObject vnuStoreCanvas;

    public GameObject JonathanKSChoi;
    public float Distance_JonathanKSChoi;
    public GameObject JonathanKSChoiCanvas;

    public GameObject daiPhunnuoc;
    public float Distance_daiPhunnuoc;
    public GameObject daiPhunnuocCanvas;

    public GameObject vnuAdminBuilding;
    public float Distance_VnuAdminBuilding;
    public GameObject vnuAdminBuildingCanvas;

    public GameObject hoitruongNVD;
    public float Distance_hoitruongNVD;
    public GameObject hoitruongNVDCanvas;

    public GameObject B2;
    public float Distance_B2;
    public GameObject B2Canvas;

    public GameObject E1;
    public float Distance_E1;
    public GameObject E1Canvas;

    public GameObject G3;
    public float Distance_G3;
    public GameObject G3Canvas;

    public GameObject E3;
    public float Distance_E3;
    public GameObject E3Canvas;

    public GameObject E4;
    public float Distance_E4;
    public GameObject E4Canvas;

    public GameObject G2;
    public float Distance_G2;
    public GameObject G2Canvas;

    public GameObject VNULic;
    public float Distance_VNULic;
    public GameObject VNULicCanvas;

    public GameObject LoveRoad;
    public float Distance_LoveRoad;
    public GameObject LoveRoadCanvas;

    public GameObject A3;
    public float Distance_A3;
    public GameObject A3Canvas;

    public GameObject A2;
    public float Distance_A2;
    public GameObject A2Canvas;

    public GameObject A1;
    public float Distance_A1;
    public GameObject A1Canvas;

    public GameObject nhathechat;
    public float Distance_nhathechat;
    public GameObject nhathechatCanvas;

    public GameObject congNN;
    public float Distance_congNN;
    public GameObject congNNCanvas;

    public GameObject congXT;
    public float Distance_congXT;
    public GameObject congXTCanvas;

    public GameObject BangTin;
    public float Distance_BangTin;
    public GameObject BangTinCanvas;

    public GameObject B1;
    public float Distance_B1;
    public GameObject B1Canvas;

    public GameObject B3;
    public float Distance_B3;
    public GameObject B3Canvas;

    public GameObject ChuyenNN;
    public float Distance_ChuyenNN;
    public GameObject ChuyenNNCanvas;

    public GameObject KTX;
    public float Distance_KTX;
    public GameObject KTXCanvas;

    public GameObject NhaAn;
    public float Distance_NhaAn;
    public GameObject NhaAnCanvas;

    public float khoangcachkyvong;   
    public GUIStyle style;
    
    // Start is called before the first frame update
    void Start()
    {
        style.fontSize = 50;
        vnuStoreCanvas.SetActive(false);
        

    }

    // Update is called once per frame
    void Update()
    {
        //Distance_ = Vector3.Distance(playerCheckPoint.transform.position, obj2.transform.position);
        //if (Distance_ < 5.0)
        //{
        //    Debug.Log("Find");
        //}
        Distance_vnuStore = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - vnuStore.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - vnuStore.transform.position.z));
        //ChessBoard distance
        if (Distance_vnuStore <= khoangcachkyvong)
        {
            Debug.Log("Find vnuStore");
            vnuStoreCanvas.SetActive(true);

        }
        else vnuStoreCanvas.SetActive(false);


        Distance_JonathanKSChoi = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - JonathanKSChoi.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - JonathanKSChoi.transform.position.z));
        //ChessBoard distance
        if (Distance_JonathanKSChoi <= khoangcachkyvong)
        {
            Debug.Log("Find JonathanKSChoi");
            JonathanKSChoiCanvas.SetActive(true);

        }
        else JonathanKSChoiCanvas.SetActive(false);


        Distance_daiPhunnuoc = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - daiPhunnuoc.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - daiPhunnuoc.transform.position.z));
        //ChessBoard distance
        if (Distance_daiPhunnuoc <= khoangcachkyvong)
        {
            Debug.Log("Find dai phun nuoc");
            daiPhunnuocCanvas.SetActive(true);

        }
        else daiPhunnuocCanvas.SetActive(false);

        //........................................................................
        Distance_VnuAdminBuilding = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - vnuAdminBuilding.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - vnuAdminBuilding.transform.position.z));
        //ChessBoard distance
        if (Distance_VnuAdminBuilding <= khoangcachkyvong)
        {
            Debug.Log("Find VNU Admin Building");
            vnuAdminBuildingCanvas.SetActive(true);

        }
        else vnuAdminBuildingCanvas.SetActive(false);


        //........................................................................
        Distance_hoitruongNVD = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - hoitruongNVD.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - hoitruongNVD.transform.position.z));
        //ChessBoard distance
        if (Distance_hoitruongNVD <= khoangcachkyvong)
        {
            Debug.Log("Find Hoi truong NVD");
            hoitruongNVDCanvas.SetActive(true);

        }
        else hoitruongNVDCanvas.SetActive(false);


        //........................................................................
        Distance_B2 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - B2.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - B2.transform.position.z));
        //ChessBoard distance
        if (Distance_B2 <= khoangcachkyvong)
        {
            Debug.Log("Find B2");
            B2Canvas.SetActive(true);
        }
        else B2Canvas.SetActive(false);


        //........................................................................
        Distance_E1 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - E1.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - E1.transform.position.z));
        //ChessBoard distance
        if (Distance_E1 <= khoangcachkyvong)
        {
            Debug.Log("Find E1");
            E1Canvas.SetActive(true);
        }
        else E1Canvas.SetActive(false);


        //........................................................................
        Distance_G3 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - G3.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - G3.transform.position.z));
        //ChessBoard distance
        if (Distance_G3 <= khoangcachkyvong)
        {
            Debug.Log("Find G3");
            G3Canvas.SetActive(true);
        }
        else G3Canvas.SetActive(false);


        //........................................................................
        Distance_E3 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - E3.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - E3.transform.position.z));
        //ChessBoard distance
        if (Distance_E3 <= khoangcachkyvong)
        {
            Debug.Log("Find E3");
            E3Canvas.SetActive(true);
        }
        else E3Canvas.SetActive(false);


        //........................................................................
        Distance_E4 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - E4.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - E4.transform.position.z));
        //ChessBoard distance
        if (Distance_E4 <= khoangcachkyvong)
        {
            Debug.Log("Find E4");
            E4Canvas.SetActive(true);
        }
        else E4Canvas.SetActive(false);

        //........................................................................
        Distance_G2 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - G2.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - G2.transform.position.z));
        //ChessBoard distance
        if (Distance_G2 <= khoangcachkyvong)
        {
            Debug.Log("Find G2");
            G2Canvas.SetActive(true);
        }
        else G2Canvas.SetActive(false);


        //........................................................................
        Distance_VNULic = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - VNULic.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - VNULic.transform.position.z));
        //ChessBoard distance
        if (Distance_VNULic <= khoangcachkyvong)
        {
            Debug.Log("Find VNU Lic");
            VNULicCanvas.SetActive(true);
        }
        else VNULicCanvas.SetActive(false);


        //........................................................................
        Distance_LoveRoad = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - LoveRoad.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - LoveRoad.transform.position.z));
        //ChessBoard distance
        if (Distance_LoveRoad <= khoangcachkyvong)
        {
            Debug.Log("Find LoveRoad");
            LoveRoadCanvas.SetActive(true);
        }
        else LoveRoadCanvas.SetActive(false);


        //........................................................................
        Distance_A3 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - A3.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - A3.transform.position.z));
        //ChessBoard distance
        if (Distance_A3 <= khoangcachkyvong)
        {
            Debug.Log("Find A3");
            A3Canvas.SetActive(true);
        }
        else A3Canvas.SetActive(false);


        //........................................................................
        Distance_A2 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - A2.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - A2.transform.position.z));
        //ChessBoard distance
        if (Distance_A2 <= khoangcachkyvong)
        {
            Debug.Log("Find A2");
            A2Canvas.SetActive(true);
        }
        else A2Canvas.SetActive(false);


        //........................................................................
        Distance_A1 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - A1.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - A1.transform.position.z));
        //ChessBoard distance
        if (Distance_A1 <= khoangcachkyvong)
        {
            Debug.Log("Find A1");
            A1Canvas.SetActive(true);
        }
        else A1Canvas.SetActive(false);


        //........................................................................
        Distance_nhathechat = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - nhathechat.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - nhathechat.transform.position.z));
        //ChessBoard distance
        if (Distance_nhathechat <= khoangcachkyvong)
        {
            Debug.Log("Find Nha the chat");
            nhathechatCanvas.SetActive(true);
        }
        else nhathechatCanvas.SetActive(false);


        //........................................................................
        Distance_congNN = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - congNN.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - congNN.transform.position.z));
        //ChessBoard distance
        if (Distance_congNN <= khoangcachkyvong)
        {
            Debug.Log("Find cong Ngoai Ngu");
            congNNCanvas.SetActive(true);
        }
        else congNNCanvas.SetActive(false);


        //........................................................................
        Distance_congXT = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - congXT.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - congXT.transform.position.z));
        //ChessBoard distance
        if (Distance_congXT <= khoangcachkyvong)
        {
            Debug.Log("Find cong Xuan Thuy");
            congXTCanvas.SetActive(true);
        }
        else congXTCanvas.SetActive(false);


        //........................................................................
        Distance_BangTin = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - BangTin.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - BangTin.transform.position.z));
        //ChessBoard distance
        if (Distance_BangTin <= khoangcachkyvong)
        {
            Debug.Log("Find Bang Tin");
            BangTinCanvas.SetActive(true);
        }
        else BangTinCanvas.SetActive(false);

        Distance_B1 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - B1.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - B1.transform.position.z));
        //ChessBoard distance
        if (Distance_B1 <= khoangcachkyvong)
        {
            Debug.Log("Find B1");
            B1Canvas.SetActive(true);
        }
        else B1Canvas.SetActive(false);

        Distance_B3 = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - B3.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - B3.transform.position.z));
        //ChessBoard distance
        if (Distance_B3 <= khoangcachkyvong)
        {
            Debug.Log("Find B3");
            B3Canvas.SetActive(true);
        }
        else B3Canvas.SetActive(false);

        Distance_ChuyenNN = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - ChuyenNN.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - ChuyenNN.transform.position.z));
        //ChessBoard distance
        if (Distance_ChuyenNN <= khoangcachkyvong)
        {
            Debug.Log("Find Chuyen Ngoai Ngu");
            ChuyenNNCanvas.SetActive(true);
        }
        else ChuyenNNCanvas.SetActive(false);

        Distance_KTX = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - KTX.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - KTX.transform.position.z));
        //ChessBoard distance
        if (Distance_KTX <= khoangcachkyvong)
        {
            Debug.Log("Find KTX");
            KTXCanvas.SetActive(true);
        }
        else KTXCanvas.SetActive(false);

        Distance_NhaAn = Mathf.Max(Mathf.Abs(playerCheckPoint.transform.position.x - NhaAn.transform.position.x), Mathf.Abs(playerCheckPoint.transform.position.z - NhaAn.transform.position.z));
        //ChessBoard distance
        if (Distance_NhaAn <= khoangcachkyvong)
        {
            Debug.Log("Find Nha An");
            NhaAnCanvas.SetActive(true);
        }
        else NhaAnCanvas.SetActive(false);


    }
    //private void OnGUI()
    //{
    //    GUI.Label(new Rect(50, 150, 200, 200), "value : " + Distance_, style);
    //}
}
