﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VNUStoreDialogueManager : MonoBehaviour
{
    public NPC npc;
    public GameObject NPCBody;
    public GameObject player;

    bool isTalking = false;

    float distance;
    float curResponseTracker = 0;
    public float khoangcachKyvong;

    public GameObject dialogueUI;

    public Text npcName;
    public Text npcDialogueBox;
    //public Text playerResponse;

    bool outOfRange;
    public GameObject dialogueGUI;
    bool dialogueUIisActive = false;
    int current = 0;
    //public float letterDelay = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        dialogueUI.SetActive(false);
        dialogueGUI.SetActive(false);

    }
    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            dialogueGUI.SetActive(false);
            dialogueUI.SetActive(false);
        }
    }
    private IEnumerator OnMouseOver()
    {
        PressEtoStartCondition();
        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - NPCBody.transform.position.x), Mathf.Abs(player.transform.position.z - NPCBody.transform.position.z));//Vector3.Distance(player.transform.position, this.transform.position);
        //Mathf.Max(Mathf.Abs(player.transform.position.x - this.transform.position.x), Mathf.Abs(player.transform.position.z - this.transform.position.z));
        if (distance <= khoangcachKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;
        if (outOfRange == false && isTalking == false)
        {
            dialogueGUI.SetActive(true);
        }
        else dialogueGUI.SetActive(false);

        if (outOfRange == false)
        {
            //trigger dialogue
            if (Input.GetKeyDown(KeyCode.E) && isTalking == false)
            {
                dialogueGUI.SetActive(false);
                StartConversation();
            }
            else if (Input.GetKeyDown(KeyCode.E) && isTalking == true)
            {
                dialogueGUI.SetActive(false);
                EndDialogue();
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                npcDialogueBox.text = npc.dialogue[current];
                Debug.Log(current);
                foreach (var letter in npcDialogueBox.text.ToCharArray())
                {
                    yield return new WaitForSeconds(1f / 200);
                }
                current++;
                if (current > npc.dialogue.Length - 1)
                {
                    current = 0;
                }
            }
        }
    }

    void StartConversation()
    {
        isTalking = true;
        curResponseTracker = 0;
        dialogueUI.SetActive(true);
        dialogueUIisActive = true;
        npcName.text = npc.name;
        npcDialogueBox.text = npc.dialogue[0];
        dialogueGUI.SetActive(false);
    }

    void EndDialogue()
    {
        isTalking = false;
        dialogueUI.SetActive(false);
        dialogueUIisActive = false;
        dialogueGUI.SetActive(false);
    }

}
