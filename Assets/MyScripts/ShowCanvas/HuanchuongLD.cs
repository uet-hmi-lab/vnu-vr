﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuanchuongLD : MonoBehaviour
{
    public GameObject player;
    public GameObject huanchuongPoint;
    public float distance;
    public float KcKyvong;

    public GameObject HuanchuongCanvas;

    bool isTalking = false;
    bool outOfRange = true;

    public GameObject canvas;
    void Start()
    {
        canvas.SetActive(false);
    }
    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            canvas.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - huanchuongPoint.transform.position.x), Mathf.Abs(player.transform.position.z - huanchuongPoint.transform.position.z));
        PressEtoStartCondition();

        if (distance <= KcKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            canvas.SetActive(true);
        }
        else canvas.SetActive(false);

        if (outOfRange == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                HuanchuongCanvas.SetActive(true);
                isTalking = true;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                isTalking = false;
                HuanchuongCanvas.SetActive(false);
            }
        }
        else
        {
            isTalking = false;
            HuanchuongCanvas.SetActive(false);
        }

    }
}
