﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loadingscene : MonoBehaviour
{
    public int nowScene;
    public int sceneWantToLoad;
    public Image _progressBar;
    public Text progressText;
    public Slider slider;
    public GameObject loadingScene;

    private void Start()
    {
        
    }
    void Update()
    {
        loadingScene.SetActive(true);
        StartCoroutine(loadAsyncOperation());
    }
    //private IEnumerator loadAsyncOperation()
    //{
    //    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneWantToLoad);
    //    while(!asyncLoad.isDone)
    //    {
    //        yield return null;
    //    }
    //}
    private IEnumerator loadAsyncOperation()
    {
        AsyncOperation gameLevel = SceneManager.LoadSceneAsync(sceneWantToLoad);

        while (!gameLevel.isDone)
        {

            float Nowprogress = Mathf.Clamp01(gameLevel.progress / 0.9f);
            //_progressBar.fillAmount = progress;
            //if(progress == 0.5f )
            //{
            //    Debug.Log("Yes");
            //}
            //Debug.Log(progress);

            slider.value = gameLevel.progress;
            progressText.text = gameLevel.progress * 100f + "%";
            //Debug.Log(progress);
            yield return null;
        }

    }
}
