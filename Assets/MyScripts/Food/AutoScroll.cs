﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AutoScroll : MonoBehaviour
{
    public GameObject player;
    public GameObject foodpoint;
    public GameObject canvas;
    public float distance;
    public float KcKyvong;

    bool isTalking = false;
    bool outOfRange = true;

    //public GameObject canvas;

    public float speed = 70f;
    float textPosBegin = -440f;
    float boundaryTextEnd = 600f;

    RectTransform myGorectTransform;
    [SerializeField]
    TextMeshProUGUI mainText;

    [SerializeField]
    public bool isLooping = false;
    // Start is called before the first frame update
    void Start()
    {
        myGorectTransform = gameObject.GetComponent<RectTransform>();
        //StartCoroutine(AutoScrollText());
        //canvas.SetActive(false);
    }
    IEnumerator AutoScrollText()
    {
        while (myGorectTransform.localPosition.y < boundaryTextEnd)
        {
            myGorectTransform.Translate(Vector3.up * speed * Time.deltaTime);
            if (myGorectTransform.localPosition.y > boundaryTextEnd)
            {
                if (isLooping)
                {
                    myGorectTransform.localPosition = Vector3.up * textPosBegin;
                }
                else
                {
                    break;
                }

            }

            yield return null;
        }
    }

    void PressEtoStartCondition()
    {
        if (outOfRange == true)
        {
            canvas.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {

        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - foodpoint.transform.position.x), Mathf.Abs(player.transform.position.z - foodpoint.transform.position.z));
        PressEtoStartCondition();

        if (distance <= KcKyvong)
        {
            outOfRange = false;
        }
        else outOfRange = true;

        if (outOfRange == false && isTalking == false)
        {
            canvas.SetActive(true);
        }
        else canvas.SetActive(false);

        if (outOfRange == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                StartCoroutine(AutoScrollText());
                isTalking = true;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                isTalking = false;
                StopCoroutine(AutoScrollText());
                myGorectTransform.localPosition = Vector3.up * textPosBegin;
                StopCoroutine(AutoScrollText());
                
            }
        }
        else
        {
            isTalking = false;
            StopCoroutine(AutoScrollText());
            myGorectTransform.localPosition = Vector3.up * textPosBegin;
            StopCoroutine(AutoScrollText());
            
        }
    }

}

