﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLeft : MonoBehaviour
{
    public GameObject player;
    public GameObject banle;
    public GameObject door;
    public float distance;
    public float KcKyvong;

    public float speed;
    public float angleLeft;
    public Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        angleLeft = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Round(banle.transform.eulerAngles.y) != angleLeft)
        {
            banle.transform.Rotate(direction * speed);
        }
        distance = Mathf.Max(Mathf.Abs(player.transform.position.x - door.transform.position.x), Mathf.Abs(player.transform.position.z - door.transform.position.z));
        if (distance < KcKyvong)
        {
            angleLeft = 270;
            direction = -Vector3.up;

        }
        if (distance >= KcKyvong)
        {
            angleLeft = 0;
            direction = Vector3.up;
        }
    }
}
