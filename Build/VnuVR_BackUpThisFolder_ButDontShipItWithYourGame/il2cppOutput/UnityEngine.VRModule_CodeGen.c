﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean UnityEngine.XR.XRSettings::get_enabled()
extern void XRSettings_get_enabled_m74A0B484E1B6D7187A34EEFFC7CDFD60E3575AA0 (void);
// 0x00000002 System.Void UnityEngine.XR.XRSettings::set_enabled(System.Boolean)
extern void XRSettings_set_enabled_m9180DFDC2B9EF502DEB9503C98A0D53B4B9F8E1B (void);
// 0x00000003 System.Boolean UnityEngine.XR.XRSettings::get_isDeviceActive()
extern void XRSettings_get_isDeviceActive_m78039C1D584FA84ED884C1D7228A8E0287BEE42E (void);
// 0x00000004 System.Single UnityEngine.XR.XRSettings::get_eyeTextureResolutionScale()
extern void XRSettings_get_eyeTextureResolutionScale_mFFBC6E9C0D8146D1B3BD579DA743A26FBA934B8E (void);
// 0x00000005 System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single)
extern void XRSettings_set_eyeTextureResolutionScale_mAB54DE7EA38F44B10B67AD0F27BB5D27BAF91253 (void);
// 0x00000006 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth()
extern void XRSettings_get_eyeTextureWidth_m214FD01E2CA4D825BDCB51AD35390BB81DFE36CD (void);
// 0x00000007 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight()
extern void XRSettings_get_eyeTextureHeight_m819C153D5D44BAB89F0AF99474AA987471B416B9 (void);
// 0x00000008 UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRSettings::get_eyeTextureDesc()
extern void XRSettings_get_eyeTextureDesc_mE8B378ED9A6692FF7E8BE261C46A426F630B69DD (void);
// 0x00000009 UnityEngine.Rendering.TextureDimension UnityEngine.XR.XRSettings::get_deviceEyeTextureDimension()
extern void XRSettings_get_deviceEyeTextureDimension_m8428FB644BB9072997A9F4F0204C1928FA88236B (void);
// 0x0000000A System.Single UnityEngine.XR.XRSettings::get_renderViewportScale()
extern void XRSettings_get_renderViewportScale_mAD4CE67ED8318B9D26CA9B092EBC592C1E38AE59 (void);
// 0x0000000B System.Void UnityEngine.XR.XRSettings::set_renderViewportScale(System.Single)
extern void XRSettings_set_renderViewportScale_mA529C7437A54660C6773267061D571EFB4F4B35D (void);
// 0x0000000C System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal()
extern void XRSettings_get_renderViewportScaleInternal_m68CF4633C56407C080DD0930AEAC9286AAA304F1 (void);
// 0x0000000D System.Void UnityEngine.XR.XRSettings::set_renderViewportScaleInternal(System.Single)
extern void XRSettings_set_renderViewportScaleInternal_m30C4242281864551E8A995B7CF5AFFD8650729EF (void);
// 0x0000000E System.String UnityEngine.XR.XRSettings::get_loadedDeviceName()
extern void XRSettings_get_loadedDeviceName_m952D46346306FD9477B13992E5797A85CCD3C98C (void);
// 0x0000000F System.Void UnityEngine.XR.XRSettings::LoadDeviceByName(System.String)
extern void XRSettings_LoadDeviceByName_m39BADCB8CB3C59E2DB929CEF7DFBF6CFA1209095 (void);
// 0x00000010 System.Void UnityEngine.XR.XRSettings::LoadDeviceByName(System.String[])
extern void XRSettings_LoadDeviceByName_m1A3EF1C6FEBD684AAC27A37470215F3D5FC54FF4 (void);
// 0x00000011 System.String[] UnityEngine.XR.XRSettings::get_supportedDevices()
extern void XRSettings_get_supportedDevices_m6FD437B8469BA7EFCEA26D254C049BCDC9766374 (void);
// 0x00000012 UnityEngine.XR.XRSettings/StereoRenderingMode UnityEngine.XR.XRSettings::get_stereoRenderingMode()
extern void XRSettings_get_stereoRenderingMode_mCCC5ED4A6AED856B344334D5860C9CA1C4ECD5C5 (void);
// 0x00000013 System.Void UnityEngine.XR.XRSettings::get_eyeTextureDesc_Injected(UnityEngine.RenderTextureDescriptor&)
extern void XRSettings_get_eyeTextureDesc_Injected_m2E5464BF666C27FB20CD0D0CCA703F40ED382597 (void);
// 0x00000014 System.Boolean UnityEngine.XR.XRDevice::get_isPresent()
extern void XRDevice_get_isPresent_m5B3D1AC4D4D14CB1AEA0FC3625B6ADE05915DDA0 (void);
// 0x00000015 System.String UnityEngine.XR.XRDevice::get_model()
extern void XRDevice_get_model_m2883ACE79611532A6385F222FE1C907B98C93C71 (void);
// 0x00000016 System.Single UnityEngine.XR.XRDevice::get_refreshRate()
extern void XRDevice_get_refreshRate_m8EE18868D630D0ED3AD10A02A0A94821D0C7DEC8 (void);
// 0x00000017 UnityEngine.XR.TrackingSpaceType UnityEngine.XR.XRDevice::GetTrackingSpaceType()
extern void XRDevice_GetTrackingSpaceType_m075417BECBBFCE6554D24E91F0E2E0264F18826C (void);
// 0x00000018 System.Boolean UnityEngine.XR.XRDevice::SetTrackingSpaceType(UnityEngine.XR.TrackingSpaceType)
extern void XRDevice_SetTrackingSpaceType_m76C173A76AD74AF048561FCEC0704F6A2E476B88 (void);
// 0x00000019 System.Void UnityEngine.XR.XRDevice::UpdateEyeTextureMSAASetting()
extern void XRDevice_UpdateEyeTextureMSAASetting_mC56D9328B079FB189E7A7F5CA97C66EFBA363165 (void);
// 0x0000001A System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_mD5D5577A4E03D0474FAFBB2596B698B6A8B5FD11 (void);
// 0x0000001B System.Void UnityEngine.XR.XRDevice::.cctor()
extern void XRDevice__cctor_m4FE111291FBDF43A481045CBABECF9AEC70B5EC9 (void);
// 0x0000001C System.Boolean UnityEngine.XR.XRStats::TryGetGPUTimeLastFrame(System.Single&)
extern void XRStats_TryGetGPUTimeLastFrame_mB03F2EFF91ED197EE78DA05320D40BFB56A387D8 (void);
// 0x0000001D System.Void UnityEngine.XR.WSA.WorldAnchor::Internal_TriggerEventOnTrackingLost(UnityEngine.XR.WSA.WorldAnchor,System.Boolean)
extern void WorldAnchor_Internal_TriggerEventOnTrackingLost_m30225CE01803079AAA7C81877ABE5A4150960B3A (void);
// 0x0000001E System.Void UnityEngine.XR.WSA.WorldAnchor/OnTrackingChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnTrackingChangedDelegate__ctor_m9CAD493361EE71D73CB61E7BFD889E88B5649497 (void);
// 0x0000001F System.Void UnityEngine.XR.WSA.WorldAnchor/OnTrackingChangedDelegate::Invoke(UnityEngine.XR.WSA.WorldAnchor,System.Boolean)
extern void OnTrackingChangedDelegate_Invoke_mC3AAC3FBDB589E9DEE9D83664191AED37F03A23C (void);
// 0x00000020 System.IAsyncResult UnityEngine.XR.WSA.WorldAnchor/OnTrackingChangedDelegate::BeginInvoke(UnityEngine.XR.WSA.WorldAnchor,System.Boolean,System.AsyncCallback,System.Object)
extern void OnTrackingChangedDelegate_BeginInvoke_mF0EFF2F8F200DEEEF063BB19BA44ACFF920D5D8C (void);
// 0x00000021 System.Void UnityEngine.XR.WSA.WorldAnchor/OnTrackingChangedDelegate::EndInvoke(System.IAsyncResult)
extern void OnTrackingChangedDelegate_EndInvoke_m00461C6664BBAAF38549748E6F2BEEBBA5F3EDD7 (void);
static Il2CppMethodPointer s_methodPointers[33] = 
{
	XRSettings_get_enabled_m74A0B484E1B6D7187A34EEFFC7CDFD60E3575AA0,
	XRSettings_set_enabled_m9180DFDC2B9EF502DEB9503C98A0D53B4B9F8E1B,
	XRSettings_get_isDeviceActive_m78039C1D584FA84ED884C1D7228A8E0287BEE42E,
	XRSettings_get_eyeTextureResolutionScale_mFFBC6E9C0D8146D1B3BD579DA743A26FBA934B8E,
	XRSettings_set_eyeTextureResolutionScale_mAB54DE7EA38F44B10B67AD0F27BB5D27BAF91253,
	XRSettings_get_eyeTextureWidth_m214FD01E2CA4D825BDCB51AD35390BB81DFE36CD,
	XRSettings_get_eyeTextureHeight_m819C153D5D44BAB89F0AF99474AA987471B416B9,
	XRSettings_get_eyeTextureDesc_mE8B378ED9A6692FF7E8BE261C46A426F630B69DD,
	XRSettings_get_deviceEyeTextureDimension_m8428FB644BB9072997A9F4F0204C1928FA88236B,
	XRSettings_get_renderViewportScale_mAD4CE67ED8318B9D26CA9B092EBC592C1E38AE59,
	XRSettings_set_renderViewportScale_mA529C7437A54660C6773267061D571EFB4F4B35D,
	XRSettings_get_renderViewportScaleInternal_m68CF4633C56407C080DD0930AEAC9286AAA304F1,
	XRSettings_set_renderViewportScaleInternal_m30C4242281864551E8A995B7CF5AFFD8650729EF,
	XRSettings_get_loadedDeviceName_m952D46346306FD9477B13992E5797A85CCD3C98C,
	XRSettings_LoadDeviceByName_m39BADCB8CB3C59E2DB929CEF7DFBF6CFA1209095,
	XRSettings_LoadDeviceByName_m1A3EF1C6FEBD684AAC27A37470215F3D5FC54FF4,
	XRSettings_get_supportedDevices_m6FD437B8469BA7EFCEA26D254C049BCDC9766374,
	XRSettings_get_stereoRenderingMode_mCCC5ED4A6AED856B344334D5860C9CA1C4ECD5C5,
	XRSettings_get_eyeTextureDesc_Injected_m2E5464BF666C27FB20CD0D0CCA703F40ED382597,
	XRDevice_get_isPresent_m5B3D1AC4D4D14CB1AEA0FC3625B6ADE05915DDA0,
	XRDevice_get_model_m2883ACE79611532A6385F222FE1C907B98C93C71,
	XRDevice_get_refreshRate_m8EE18868D630D0ED3AD10A02A0A94821D0C7DEC8,
	XRDevice_GetTrackingSpaceType_m075417BECBBFCE6554D24E91F0E2E0264F18826C,
	XRDevice_SetTrackingSpaceType_m76C173A76AD74AF048561FCEC0704F6A2E476B88,
	XRDevice_UpdateEyeTextureMSAASetting_mC56D9328B079FB189E7A7F5CA97C66EFBA363165,
	XRDevice_InvokeDeviceLoaded_mD5D5577A4E03D0474FAFBB2596B698B6A8B5FD11,
	XRDevice__cctor_m4FE111291FBDF43A481045CBABECF9AEC70B5EC9,
	XRStats_TryGetGPUTimeLastFrame_mB03F2EFF91ED197EE78DA05320D40BFB56A387D8,
	WorldAnchor_Internal_TriggerEventOnTrackingLost_m30225CE01803079AAA7C81877ABE5A4150960B3A,
	OnTrackingChangedDelegate__ctor_m9CAD493361EE71D73CB61E7BFD889E88B5649497,
	OnTrackingChangedDelegate_Invoke_mC3AAC3FBDB589E9DEE9D83664191AED37F03A23C,
	OnTrackingChangedDelegate_BeginInvoke_mF0EFF2F8F200DEEEF063BB19BA44ACFF920D5D8C,
	OnTrackingChangedDelegate_EndInvoke_m00461C6664BBAAF38549748E6F2BEEBBA5F3EDD7,
};
static const int32_t s_InvokerIndices[33] = 
{
	49,
	804,
	49,
	1167,
	1347,
	131,
	131,
	1834,
	131,
	1167,
	1347,
	1167,
	1347,
	4,
	122,
	122,
	4,
	131,
	17,
	49,
	4,
	1167,
	131,
	46,
	3,
	122,
	3,
	347,
	573,
	102,
	410,
	794,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_VRModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModuleCodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	33,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
