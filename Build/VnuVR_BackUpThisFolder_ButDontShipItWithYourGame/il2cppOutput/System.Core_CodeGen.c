﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::Format(System.String,System.Object)
extern void SR_Format_mC35B52FE843D9C9D465B6B544FA184058A46E0A9 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000003 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000004 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000005 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000006 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000E System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000F System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000014 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000016 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DefaultIfEmpty(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DefaultIfEmpty(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DefaultIfEmptyIterator(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000001C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000001D TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001F TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000020 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000021 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000022 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000023 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000026 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000028 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000029 System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88 (void);
// 0x0000002A System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000002B System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x0000002C TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x0000002D System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x0000002E System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x0000002F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000030 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000031 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000034 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000035 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000036 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000038 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000039 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000042 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000043 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000046 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000047 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000048 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000049 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000004A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004C System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000004D System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x0000004E System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x0000004F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000051 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000052 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x00000053 System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000054 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000056 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000057 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000058 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000059 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000005A System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000005B System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000005C System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x0000005D System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000005E System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000005F TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000060 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000061 System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000062 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000063 System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000064 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x00000065 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x00000066 System.Boolean System.Linq.Enumerable/<SkipIterator>d__31`1::MoveNext()
// 0x00000067 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::<>m__Finally1()
// 0x00000068 TSource System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000069 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x0000006A System.Object System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x0000006B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000006C System.Collections.IEnumerator System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006D System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x0000006E System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x0000006F System.Boolean System.Linq.Enumerable/<ConcatIterator>d__59`1::MoveNext()
// 0x00000070 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally1()
// 0x00000071 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally2()
// 0x00000072 TSource System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000073 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x00000074 System.Object System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x00000075 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000076 System.Collections.IEnumerator System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000077 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000078 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000079 System.Boolean System.Linq.Enumerable/<DistinctIterator>d__68`1::MoveNext()
// 0x0000007A System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000007B TSource System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000007C System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000007D System.Object System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000007E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000007F System.Collections.IEnumerator System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000080 System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::.ctor(System.Int32)
// 0x00000081 System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::System.IDisposable.Dispose()
// 0x00000082 System.Boolean System.Linq.Enumerable/<ExceptIterator>d__77`1::MoveNext()
// 0x00000083 System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::<>m__Finally1()
// 0x00000084 TSource System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000085 System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.IEnumerator.Reset()
// 0x00000086 System.Object System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.IEnumerator.get_Current()
// 0x00000087 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000088 System.Collections.IEnumerator System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000089 System.Void System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::.ctor(System.Int32)
// 0x0000008A System.Void System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::System.IDisposable.Dispose()
// 0x0000008B System.Boolean System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::MoveNext()
// 0x0000008C System.Void System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::<>m__Finally1()
// 0x0000008D TSource System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008E System.Void System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Enumerable/<DefaultIfEmptyIterator>d__95`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000093 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000094 System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x00000095 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x00000096 TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000097 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000098 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000099 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000009A System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009B System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000009C System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000009D System.Boolean System.Linq.Set`1::Add(TElement)
// 0x0000009E System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x0000009F System.Void System.Linq.Set`1::Resize()
// 0x000000A0 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000A1 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000A2 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A3 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A4 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000A5 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000A6 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000A7 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000A8 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x000000A9 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000AA System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000AB System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000AC System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000AD System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000AE System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000AF System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000B0 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000B1 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000B2 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000B3 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000B4 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000B5 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000B6 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000B7 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000B8 System.Linq.Expressions.ExpressionType System.Linq.Expressions.Expression::get_NodeType()
extern void Expression_get_NodeType_m230B6FE36D83B480D5ABBD01A1BA27A6F33BFCE3 (void);
// 0x000000B9 System.Void System.Linq.Expressions.Expression::.cctor()
extern void Expression__cctor_m6C47FA07B808E8507CCCEACDCFE8EE668AEC409C (void);
// 0x000000BA System.Exception System.Linq.Expressions.Error::ExtensionNodeMustOverrideProperty(System.Object)
extern void Error_ExtensionNodeMustOverrideProperty_mB79A086A50C7F12802A53324D7AF8C4E7EE208C6 (void);
// 0x000000BB System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::get_Body()
extern void LambdaExpression_get_Body_m7D6D43418EE260E6523847D5C9E866FC466C89D0 (void);
// 0x000000BC System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::get_Member()
extern void MemberExpression_get_Member_m28610D227E14379794673549E914931312AA1272 (void);
// 0x000000BD System.Linq.Expressions.Expression System.Linq.Expressions.MemberExpression::get_Expression()
extern void MemberExpression_get_Expression_m5FD4FC0BEE3268821D97C01C87BEB0C1F9D26351 (void);
// 0x000000BE System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::GetMember()
extern void MemberExpression_GetMember_m35CBBB03CFAFC32A700D877C35A4CD70C8C3FCA2 (void);
// 0x000000BF System.String System.Linq.Expressions.Strings::ExtensionNodeMustOverrideProperty(System.Object)
extern void Strings_ExtensionNodeMustOverrideProperty_m2468ED1C894BD378104B522523876152F9D1F0D0 (void);
// 0x000000C0 System.Void System.Dynamic.Utils.CacheDict`2::.ctor(System.Int32)
// 0x000000C1 System.Int32 System.Dynamic.Utils.CacheDict`2::AlignSize(System.Int32)
// 0x000000C2 System.Exception System.Dynamic.Utils.ContractUtils::get_Unreachable()
extern void ContractUtils_get_Unreachable_mE78FFD6E65C4EB6EA0FB53A98EBE5AA477309E2F (void);
// 0x000000C3 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F (void);
// 0x000000C4 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5 (void);
// 0x000000C5 System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802 (void);
// 0x000000C6 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345 (void);
// 0x000000C7 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991 (void);
// 0x000000C8 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000C9 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000CA System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000CB System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000CC System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000CD System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000CE System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000CF System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000D0 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000D2 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000D3 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000D4 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000D5 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000D6 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000D7 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D8 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000D9 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000DA System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000DB System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000DC System.Boolean System.Collections.Generic.HashSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000DD System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000DE System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000DF System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000E0 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000E4 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000E6 System.Boolean System.Collections.Generic.HashSet`1::ContainsAllElements(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E7 System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x000000E8 System.Collections.Generic.HashSet`1/ElementCount<T> System.Collections.Generic.HashSet`1::CheckUniqueAndUnfoundElements(System.Collections.Generic.IEnumerable`1<T>,System.Boolean)
// 0x000000E9 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000EA System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000EB System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000EC System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x000000ED System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x000000EE T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x000000EF System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000F0 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[240] = 
{
	SR_Format_mC35B52FE843D9C9D465B6B544FA184058A46E0A9,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Expression_get_NodeType_m230B6FE36D83B480D5ABBD01A1BA27A6F33BFCE3,
	Expression__cctor_m6C47FA07B808E8507CCCEACDCFE8EE668AEC409C,
	Error_ExtensionNodeMustOverrideProperty_mB79A086A50C7F12802A53324D7AF8C4E7EE208C6,
	LambdaExpression_get_Body_m7D6D43418EE260E6523847D5C9E866FC466C89D0,
	MemberExpression_get_Member_m28610D227E14379794673549E914931312AA1272,
	MemberExpression_get_Expression_m5FD4FC0BEE3268821D97C01C87BEB0C1F9D26351,
	MemberExpression_GetMember_m35CBBB03CFAFC32A700D877C35A4CD70C8C3FCA2,
	Strings_ExtensionNodeMustOverrideProperty_m2468ED1C894BD378104B522523876152F9D1F0D0,
	NULL,
	NULL,
	ContractUtils_get_Unreachable_mE78FFD6E65C4EB6EA0FB53A98EBE5AA477309E2F,
	BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F,
	BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5,
	BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802,
	BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345,
	BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[240] = 
{
	1,
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	95,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	10,
	3,
	0,
	14,
	14,
	14,
	14,
	0,
	-1,
	-1,
	4,
	64,
	140,
	32,
	30,
	21,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[69] = 
{
	{ 0x02000005, { 98, 4 } },
	{ 0x02000006, { 102, 9 } },
	{ 0x02000007, { 113, 7 } },
	{ 0x02000008, { 122, 10 } },
	{ 0x02000009, { 134, 11 } },
	{ 0x0200000A, { 148, 9 } },
	{ 0x0200000B, { 160, 12 } },
	{ 0x0200000C, { 175, 1 } },
	{ 0x0200000D, { 176, 2 } },
	{ 0x0200000E, { 178, 12 } },
	{ 0x0200000F, { 190, 8 } },
	{ 0x02000010, { 198, 9 } },
	{ 0x02000011, { 207, 11 } },
	{ 0x02000012, { 218, 11 } },
	{ 0x02000013, { 229, 8 } },
	{ 0x02000014, { 237, 6 } },
	{ 0x02000016, { 243, 8 } },
	{ 0x02000018, { 251, 3 } },
	{ 0x02000019, { 256, 5 } },
	{ 0x0200001A, { 261, 7 } },
	{ 0x0200001B, { 268, 3 } },
	{ 0x0200001C, { 271, 7 } },
	{ 0x0200001D, { 278, 4 } },
	{ 0x02000028, { 282, 3 } },
	{ 0x0200002C, { 285, 39 } },
	{ 0x0200002F, { 324, 2 } },
	{ 0x06000006, { 0, 10 } },
	{ 0x06000007, { 10, 10 } },
	{ 0x06000008, { 20, 5 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 1 } },
	{ 0x0600000B, { 31, 2 } },
	{ 0x0600000C, { 33, 1 } },
	{ 0x0600000D, { 34, 2 } },
	{ 0x0600000E, { 36, 2 } },
	{ 0x0600000F, { 38, 1 } },
	{ 0x06000010, { 39, 1 } },
	{ 0x06000011, { 40, 2 } },
	{ 0x06000012, { 42, 1 } },
	{ 0x06000013, { 43, 2 } },
	{ 0x06000014, { 45, 1 } },
	{ 0x06000015, { 46, 2 } },
	{ 0x06000016, { 48, 3 } },
	{ 0x06000017, { 51, 2 } },
	{ 0x06000018, { 53, 1 } },
	{ 0x06000019, { 54, 1 } },
	{ 0x0600001A, { 55, 2 } },
	{ 0x0600001B, { 57, 2 } },
	{ 0x0600001C, { 59, 2 } },
	{ 0x0600001D, { 61, 4 } },
	{ 0x0600001E, { 65, 3 } },
	{ 0x0600001F, { 68, 4 } },
	{ 0x06000020, { 72, 3 } },
	{ 0x06000021, { 75, 3 } },
	{ 0x06000022, { 78, 1 } },
	{ 0x06000023, { 79, 3 } },
	{ 0x06000024, { 82, 3 } },
	{ 0x06000025, { 85, 2 } },
	{ 0x06000026, { 87, 3 } },
	{ 0x06000027, { 90, 2 } },
	{ 0x06000028, { 92, 5 } },
	{ 0x0600002A, { 97, 1 } },
	{ 0x0600003A, { 111, 2 } },
	{ 0x0600003F, { 120, 2 } },
	{ 0x06000044, { 132, 2 } },
	{ 0x0600004A, { 145, 3 } },
	{ 0x0600004F, { 157, 3 } },
	{ 0x06000054, { 172, 3 } },
	{ 0x060000A4, { 254, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[326] = 
{
	{ (Il2CppRGCTXDataType)2, 28627 },
	{ (Il2CppRGCTXDataType)3, 22576 },
	{ (Il2CppRGCTXDataType)2, 28628 },
	{ (Il2CppRGCTXDataType)2, 28629 },
	{ (Il2CppRGCTXDataType)3, 22577 },
	{ (Il2CppRGCTXDataType)2, 28630 },
	{ (Il2CppRGCTXDataType)2, 28631 },
	{ (Il2CppRGCTXDataType)3, 22578 },
	{ (Il2CppRGCTXDataType)2, 28632 },
	{ (Il2CppRGCTXDataType)3, 22579 },
	{ (Il2CppRGCTXDataType)2, 28633 },
	{ (Il2CppRGCTXDataType)3, 22580 },
	{ (Il2CppRGCTXDataType)2, 28634 },
	{ (Il2CppRGCTXDataType)2, 28635 },
	{ (Il2CppRGCTXDataType)3, 22581 },
	{ (Il2CppRGCTXDataType)2, 28636 },
	{ (Il2CppRGCTXDataType)2, 28637 },
	{ (Il2CppRGCTXDataType)3, 22582 },
	{ (Il2CppRGCTXDataType)2, 28638 },
	{ (Il2CppRGCTXDataType)3, 22583 },
	{ (Il2CppRGCTXDataType)2, 28639 },
	{ (Il2CppRGCTXDataType)3, 22584 },
	{ (Il2CppRGCTXDataType)3, 22585 },
	{ (Il2CppRGCTXDataType)2, 20243 },
	{ (Il2CppRGCTXDataType)3, 22586 },
	{ (Il2CppRGCTXDataType)2, 28640 },
	{ (Il2CppRGCTXDataType)3, 22587 },
	{ (Il2CppRGCTXDataType)3, 22588 },
	{ (Il2CppRGCTXDataType)2, 20250 },
	{ (Il2CppRGCTXDataType)3, 22589 },
	{ (Il2CppRGCTXDataType)3, 22590 },
	{ (Il2CppRGCTXDataType)2, 28641 },
	{ (Il2CppRGCTXDataType)3, 22591 },
	{ (Il2CppRGCTXDataType)3, 22592 },
	{ (Il2CppRGCTXDataType)2, 28642 },
	{ (Il2CppRGCTXDataType)3, 22593 },
	{ (Il2CppRGCTXDataType)2, 28643 },
	{ (Il2CppRGCTXDataType)3, 22594 },
	{ (Il2CppRGCTXDataType)3, 22595 },
	{ (Il2CppRGCTXDataType)3, 22596 },
	{ (Il2CppRGCTXDataType)2, 28644 },
	{ (Il2CppRGCTXDataType)3, 22597 },
	{ (Il2CppRGCTXDataType)3, 22598 },
	{ (Il2CppRGCTXDataType)2, 28645 },
	{ (Il2CppRGCTXDataType)3, 22599 },
	{ (Il2CppRGCTXDataType)3, 22600 },
	{ (Il2CppRGCTXDataType)2, 28646 },
	{ (Il2CppRGCTXDataType)3, 22601 },
	{ (Il2CppRGCTXDataType)2, 28647 },
	{ (Il2CppRGCTXDataType)3, 22602 },
	{ (Il2CppRGCTXDataType)3, 22603 },
	{ (Il2CppRGCTXDataType)2, 20293 },
	{ (Il2CppRGCTXDataType)3, 22604 },
	{ (Il2CppRGCTXDataType)3, 22605 },
	{ (Il2CppRGCTXDataType)3, 22606 },
	{ (Il2CppRGCTXDataType)2, 28648 },
	{ (Il2CppRGCTXDataType)3, 22607 },
	{ (Il2CppRGCTXDataType)2, 20300 },
	{ (Il2CppRGCTXDataType)3, 22608 },
	{ (Il2CppRGCTXDataType)2, 28649 },
	{ (Il2CppRGCTXDataType)3, 22609 },
	{ (Il2CppRGCTXDataType)2, 28650 },
	{ (Il2CppRGCTXDataType)2, 28651 },
	{ (Il2CppRGCTXDataType)2, 20304 },
	{ (Il2CppRGCTXDataType)2, 28652 },
	{ (Il2CppRGCTXDataType)2, 20306 },
	{ (Il2CppRGCTXDataType)2, 28653 },
	{ (Il2CppRGCTXDataType)3, 22610 },
	{ (Il2CppRGCTXDataType)2, 28654 },
	{ (Il2CppRGCTXDataType)2, 28655 },
	{ (Il2CppRGCTXDataType)2, 20309 },
	{ (Il2CppRGCTXDataType)2, 28656 },
	{ (Il2CppRGCTXDataType)2, 20311 },
	{ (Il2CppRGCTXDataType)2, 28657 },
	{ (Il2CppRGCTXDataType)3, 22611 },
	{ (Il2CppRGCTXDataType)2, 28658 },
	{ (Il2CppRGCTXDataType)2, 20314 },
	{ (Il2CppRGCTXDataType)2, 28659 },
	{ (Il2CppRGCTXDataType)2, 20316 },
	{ (Il2CppRGCTXDataType)2, 20318 },
	{ (Il2CppRGCTXDataType)2, 28660 },
	{ (Il2CppRGCTXDataType)3, 22612 },
	{ (Il2CppRGCTXDataType)2, 20321 },
	{ (Il2CppRGCTXDataType)2, 28661 },
	{ (Il2CppRGCTXDataType)3, 22613 },
	{ (Il2CppRGCTXDataType)2, 28662 },
	{ (Il2CppRGCTXDataType)2, 20324 },
	{ (Il2CppRGCTXDataType)2, 20326 },
	{ (Il2CppRGCTXDataType)2, 28663 },
	{ (Il2CppRGCTXDataType)3, 22614 },
	{ (Il2CppRGCTXDataType)2, 28664 },
	{ (Il2CppRGCTXDataType)3, 22615 },
	{ (Il2CppRGCTXDataType)3, 22616 },
	{ (Il2CppRGCTXDataType)2, 28665 },
	{ (Il2CppRGCTXDataType)2, 20331 },
	{ (Il2CppRGCTXDataType)2, 28666 },
	{ (Il2CppRGCTXDataType)2, 20333 },
	{ (Il2CppRGCTXDataType)3, 22617 },
	{ (Il2CppRGCTXDataType)3, 22618 },
	{ (Il2CppRGCTXDataType)3, 22619 },
	{ (Il2CppRGCTXDataType)2, 20339 },
	{ (Il2CppRGCTXDataType)3, 22620 },
	{ (Il2CppRGCTXDataType)3, 22621 },
	{ (Il2CppRGCTXDataType)2, 20351 },
	{ (Il2CppRGCTXDataType)2, 28667 },
	{ (Il2CppRGCTXDataType)3, 22622 },
	{ (Il2CppRGCTXDataType)3, 22623 },
	{ (Il2CppRGCTXDataType)2, 20353 },
	{ (Il2CppRGCTXDataType)2, 28491 },
	{ (Il2CppRGCTXDataType)3, 22624 },
	{ (Il2CppRGCTXDataType)3, 22625 },
	{ (Il2CppRGCTXDataType)2, 28668 },
	{ (Il2CppRGCTXDataType)3, 22626 },
	{ (Il2CppRGCTXDataType)3, 22627 },
	{ (Il2CppRGCTXDataType)2, 20363 },
	{ (Il2CppRGCTXDataType)2, 28669 },
	{ (Il2CppRGCTXDataType)3, 22628 },
	{ (Il2CppRGCTXDataType)3, 22629 },
	{ (Il2CppRGCTXDataType)3, 21206 },
	{ (Il2CppRGCTXDataType)3, 22630 },
	{ (Il2CppRGCTXDataType)2, 28670 },
	{ (Il2CppRGCTXDataType)3, 22631 },
	{ (Il2CppRGCTXDataType)3, 22632 },
	{ (Il2CppRGCTXDataType)2, 20375 },
	{ (Il2CppRGCTXDataType)2, 28671 },
	{ (Il2CppRGCTXDataType)3, 22633 },
	{ (Il2CppRGCTXDataType)3, 22634 },
	{ (Il2CppRGCTXDataType)3, 22635 },
	{ (Il2CppRGCTXDataType)3, 22636 },
	{ (Il2CppRGCTXDataType)3, 22637 },
	{ (Il2CppRGCTXDataType)3, 21212 },
	{ (Il2CppRGCTXDataType)3, 22638 },
	{ (Il2CppRGCTXDataType)2, 28672 },
	{ (Il2CppRGCTXDataType)3, 22639 },
	{ (Il2CppRGCTXDataType)3, 22640 },
	{ (Il2CppRGCTXDataType)2, 20388 },
	{ (Il2CppRGCTXDataType)2, 28673 },
	{ (Il2CppRGCTXDataType)3, 22641 },
	{ (Il2CppRGCTXDataType)3, 22642 },
	{ (Il2CppRGCTXDataType)2, 20390 },
	{ (Il2CppRGCTXDataType)2, 28674 },
	{ (Il2CppRGCTXDataType)3, 22643 },
	{ (Il2CppRGCTXDataType)3, 22644 },
	{ (Il2CppRGCTXDataType)2, 28675 },
	{ (Il2CppRGCTXDataType)3, 22645 },
	{ (Il2CppRGCTXDataType)3, 22646 },
	{ (Il2CppRGCTXDataType)2, 28676 },
	{ (Il2CppRGCTXDataType)3, 22647 },
	{ (Il2CppRGCTXDataType)3, 22648 },
	{ (Il2CppRGCTXDataType)2, 20405 },
	{ (Il2CppRGCTXDataType)2, 28677 },
	{ (Il2CppRGCTXDataType)3, 22649 },
	{ (Il2CppRGCTXDataType)3, 22650 },
	{ (Il2CppRGCTXDataType)3, 22651 },
	{ (Il2CppRGCTXDataType)3, 21223 },
	{ (Il2CppRGCTXDataType)2, 28678 },
	{ (Il2CppRGCTXDataType)3, 22652 },
	{ (Il2CppRGCTXDataType)3, 22653 },
	{ (Il2CppRGCTXDataType)2, 28679 },
	{ (Il2CppRGCTXDataType)3, 22654 },
	{ (Il2CppRGCTXDataType)3, 22655 },
	{ (Il2CppRGCTXDataType)2, 20421 },
	{ (Il2CppRGCTXDataType)2, 28680 },
	{ (Il2CppRGCTXDataType)3, 22656 },
	{ (Il2CppRGCTXDataType)3, 22657 },
	{ (Il2CppRGCTXDataType)3, 22658 },
	{ (Il2CppRGCTXDataType)3, 22659 },
	{ (Il2CppRGCTXDataType)3, 22660 },
	{ (Il2CppRGCTXDataType)3, 22661 },
	{ (Il2CppRGCTXDataType)3, 21229 },
	{ (Il2CppRGCTXDataType)2, 28681 },
	{ (Il2CppRGCTXDataType)3, 22662 },
	{ (Il2CppRGCTXDataType)3, 22663 },
	{ (Il2CppRGCTXDataType)2, 28682 },
	{ (Il2CppRGCTXDataType)3, 22664 },
	{ (Il2CppRGCTXDataType)3, 22665 },
	{ (Il2CppRGCTXDataType)3, 22666 },
	{ (Il2CppRGCTXDataType)3, 22667 },
	{ (Il2CppRGCTXDataType)3, 22668 },
	{ (Il2CppRGCTXDataType)3, 22669 },
	{ (Il2CppRGCTXDataType)2, 28683 },
	{ (Il2CppRGCTXDataType)2, 28684 },
	{ (Il2CppRGCTXDataType)3, 22670 },
	{ (Il2CppRGCTXDataType)2, 20456 },
	{ (Il2CppRGCTXDataType)2, 20450 },
	{ (Il2CppRGCTXDataType)3, 22671 },
	{ (Il2CppRGCTXDataType)2, 20449 },
	{ (Il2CppRGCTXDataType)2, 28685 },
	{ (Il2CppRGCTXDataType)3, 22672 },
	{ (Il2CppRGCTXDataType)3, 22673 },
	{ (Il2CppRGCTXDataType)3, 22674 },
	{ (Il2CppRGCTXDataType)2, 20469 },
	{ (Il2CppRGCTXDataType)2, 20464 },
	{ (Il2CppRGCTXDataType)3, 22675 },
	{ (Il2CppRGCTXDataType)2, 20463 },
	{ (Il2CppRGCTXDataType)2, 28686 },
	{ (Il2CppRGCTXDataType)3, 22676 },
	{ (Il2CppRGCTXDataType)3, 22677 },
	{ (Il2CppRGCTXDataType)3, 22678 },
	{ (Il2CppRGCTXDataType)3, 22679 },
	{ (Il2CppRGCTXDataType)2, 20479 },
	{ (Il2CppRGCTXDataType)2, 20474 },
	{ (Il2CppRGCTXDataType)3, 22680 },
	{ (Il2CppRGCTXDataType)2, 20473 },
	{ (Il2CppRGCTXDataType)2, 28687 },
	{ (Il2CppRGCTXDataType)3, 22681 },
	{ (Il2CppRGCTXDataType)3, 22682 },
	{ (Il2CppRGCTXDataType)3, 22683 },
	{ (Il2CppRGCTXDataType)2, 28688 },
	{ (Il2CppRGCTXDataType)3, 22684 },
	{ (Il2CppRGCTXDataType)2, 20492 },
	{ (Il2CppRGCTXDataType)2, 20484 },
	{ (Il2CppRGCTXDataType)3, 22685 },
	{ (Il2CppRGCTXDataType)3, 22686 },
	{ (Il2CppRGCTXDataType)2, 20483 },
	{ (Il2CppRGCTXDataType)2, 28689 },
	{ (Il2CppRGCTXDataType)3, 22687 },
	{ (Il2CppRGCTXDataType)3, 22688 },
	{ (Il2CppRGCTXDataType)3, 22689 },
	{ (Il2CppRGCTXDataType)2, 28690 },
	{ (Il2CppRGCTXDataType)3, 22690 },
	{ (Il2CppRGCTXDataType)2, 20505 },
	{ (Il2CppRGCTXDataType)2, 20497 },
	{ (Il2CppRGCTXDataType)3, 22691 },
	{ (Il2CppRGCTXDataType)3, 22692 },
	{ (Il2CppRGCTXDataType)2, 20496 },
	{ (Il2CppRGCTXDataType)2, 28691 },
	{ (Il2CppRGCTXDataType)3, 22693 },
	{ (Il2CppRGCTXDataType)3, 22694 },
	{ (Il2CppRGCTXDataType)3, 22695 },
	{ (Il2CppRGCTXDataType)2, 20516 },
	{ (Il2CppRGCTXDataType)2, 20510 },
	{ (Il2CppRGCTXDataType)3, 22696 },
	{ (Il2CppRGCTXDataType)2, 20509 },
	{ (Il2CppRGCTXDataType)2, 28692 },
	{ (Il2CppRGCTXDataType)3, 22697 },
	{ (Il2CppRGCTXDataType)3, 22698 },
	{ (Il2CppRGCTXDataType)3, 22699 },
	{ (Il2CppRGCTXDataType)2, 20520 },
	{ (Il2CppRGCTXDataType)3, 22700 },
	{ (Il2CppRGCTXDataType)2, 28693 },
	{ (Il2CppRGCTXDataType)3, 22701 },
	{ (Il2CppRGCTXDataType)3, 22702 },
	{ (Il2CppRGCTXDataType)3, 22703 },
	{ (Il2CppRGCTXDataType)2, 28694 },
	{ (Il2CppRGCTXDataType)2, 28695 },
	{ (Il2CppRGCTXDataType)3, 22704 },
	{ (Il2CppRGCTXDataType)3, 22705 },
	{ (Il2CppRGCTXDataType)2, 20536 },
	{ (Il2CppRGCTXDataType)3, 22706 },
	{ (Il2CppRGCTXDataType)2, 20537 },
	{ (Il2CppRGCTXDataType)2, 28696 },
	{ (Il2CppRGCTXDataType)3, 22707 },
	{ (Il2CppRGCTXDataType)3, 22708 },
	{ (Il2CppRGCTXDataType)2, 28697 },
	{ (Il2CppRGCTXDataType)3, 22709 },
	{ (Il2CppRGCTXDataType)2, 28698 },
	{ (Il2CppRGCTXDataType)3, 22710 },
	{ (Il2CppRGCTXDataType)3, 22711 },
	{ (Il2CppRGCTXDataType)3, 22712 },
	{ (Il2CppRGCTXDataType)2, 20556 },
	{ (Il2CppRGCTXDataType)3, 22713 },
	{ (Il2CppRGCTXDataType)2, 20564 },
	{ (Il2CppRGCTXDataType)3, 22714 },
	{ (Il2CppRGCTXDataType)2, 28699 },
	{ (Il2CppRGCTXDataType)2, 28700 },
	{ (Il2CppRGCTXDataType)3, 22715 },
	{ (Il2CppRGCTXDataType)3, 22716 },
	{ (Il2CppRGCTXDataType)3, 22717 },
	{ (Il2CppRGCTXDataType)3, 22718 },
	{ (Il2CppRGCTXDataType)3, 22719 },
	{ (Il2CppRGCTXDataType)3, 22720 },
	{ (Il2CppRGCTXDataType)2, 20580 },
	{ (Il2CppRGCTXDataType)2, 28701 },
	{ (Il2CppRGCTXDataType)3, 22721 },
	{ (Il2CppRGCTXDataType)3, 22722 },
	{ (Il2CppRGCTXDataType)2, 20584 },
	{ (Il2CppRGCTXDataType)3, 22723 },
	{ (Il2CppRGCTXDataType)2, 28702 },
	{ (Il2CppRGCTXDataType)2, 20594 },
	{ (Il2CppRGCTXDataType)2, 20592 },
	{ (Il2CppRGCTXDataType)2, 28703 },
	{ (Il2CppRGCTXDataType)3, 22724 },
	{ (Il2CppRGCTXDataType)2, 28704 },
	{ (Il2CppRGCTXDataType)2, 28705 },
	{ (Il2CppRGCTXDataType)3, 22725 },
	{ (Il2CppRGCTXDataType)2, 28706 },
	{ (Il2CppRGCTXDataType)3, 22726 },
	{ (Il2CppRGCTXDataType)3, 22727 },
	{ (Il2CppRGCTXDataType)2, 20638 },
	{ (Il2CppRGCTXDataType)3, 22728 },
	{ (Il2CppRGCTXDataType)2, 20638 },
	{ (Il2CppRGCTXDataType)3, 22729 },
	{ (Il2CppRGCTXDataType)2, 20658 },
	{ (Il2CppRGCTXDataType)3, 22730 },
	{ (Il2CppRGCTXDataType)3, 22731 },
	{ (Il2CppRGCTXDataType)3, 22732 },
	{ (Il2CppRGCTXDataType)2, 28707 },
	{ (Il2CppRGCTXDataType)3, 22733 },
	{ (Il2CppRGCTXDataType)3, 22734 },
	{ (Il2CppRGCTXDataType)3, 22735 },
	{ (Il2CppRGCTXDataType)2, 20635 },
	{ (Il2CppRGCTXDataType)3, 22736 },
	{ (Il2CppRGCTXDataType)3, 22737 },
	{ (Il2CppRGCTXDataType)2, 20640 },
	{ (Il2CppRGCTXDataType)3, 22738 },
	{ (Il2CppRGCTXDataType)1, 28708 },
	{ (Il2CppRGCTXDataType)2, 20639 },
	{ (Il2CppRGCTXDataType)3, 22739 },
	{ (Il2CppRGCTXDataType)1, 20639 },
	{ (Il2CppRGCTXDataType)1, 20635 },
	{ (Il2CppRGCTXDataType)2, 28707 },
	{ (Il2CppRGCTXDataType)2, 20639 },
	{ (Il2CppRGCTXDataType)2, 20637 },
	{ (Il2CppRGCTXDataType)2, 20641 },
	{ (Il2CppRGCTXDataType)3, 22740 },
	{ (Il2CppRGCTXDataType)3, 22741 },
	{ (Il2CppRGCTXDataType)3, 22742 },
	{ (Il2CppRGCTXDataType)3, 22743 },
	{ (Il2CppRGCTXDataType)3, 22744 },
	{ (Il2CppRGCTXDataType)3, 22745 },
	{ (Il2CppRGCTXDataType)3, 22746 },
	{ (Il2CppRGCTXDataType)3, 22747 },
	{ (Il2CppRGCTXDataType)2, 20636 },
	{ (Il2CppRGCTXDataType)3, 22748 },
	{ (Il2CppRGCTXDataType)2, 20654 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	240,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	69,
	s_rgctxIndices,
	326,
	s_rgctxValues,
	NULL,
};
