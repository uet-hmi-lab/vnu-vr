﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Single UnityEngine.SoftJointLimit::get_limit()
extern void SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03 (void);
// 0x00000002 System.Void UnityEngine.SoftJointLimit::set_limit(System.Single)
extern void SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26 (void);
// 0x00000003 System.Void UnityEngine.JointDrive::set_positionSpring(System.Single)
extern void JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8 (void);
// 0x00000004 System.Void UnityEngine.JointDrive::set_positionDamper(System.Single)
extern void JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF (void);
// 0x00000005 System.Void UnityEngine.JointDrive::set_maximumForce(System.Single)
extern void JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945 (void);
// 0x00000006 System.Single UnityEngine.JointLimits::get_min()
extern void JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001 (void);
// 0x00000007 System.Void UnityEngine.JointLimits::set_min(System.Single)
extern void JointLimits_set_min_mBEF6F81D32295EE572F03C59CC28F13ECA322DE4 (void);
// 0x00000008 System.Single UnityEngine.JointLimits::get_max()
extern void JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F (void);
// 0x00000009 System.Void UnityEngine.JointLimits::set_max(System.Single)
extern void JointLimits_set_max_m71FC88C4B4720B81128A9906CD5E1D018D207FB8 (void);
// 0x0000000A System.Void UnityEngine.JointLimits::set_bounciness(System.Single)
extern void JointLimits_set_bounciness_mD9CF3456A5E3E46FD36481A6E3D76F91505D12DC (void);
// 0x0000000B UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern void Collision_get_relativeVelocity_m0FB1600821CDCD3C52D56F80304A509FC1DF702E (void);
// 0x0000000C UnityEngine.Collider UnityEngine.Collision::get_collider()
extern void Collision_get_collider_m52F32CFE0BC1925C72A5B8EB743BBEF628201352 (void);
// 0x0000000D UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern void Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49 (void);
// 0x0000000E UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern void Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF (void);
// 0x0000000F UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern void RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9 (void);
// 0x00000010 UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern void RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001 (void);
// 0x00000011 UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern void RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94 (void);
// 0x00000012 System.Single UnityEngine.RaycastHit::get_distance()
extern void RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543 (void);
// 0x00000013 UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern void RaycastHit_get_transform_m3C0BEE7439CA37F82FD5216143B92BF32F995279 (void);
// 0x00000014 UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern void RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC (void);
// 0x00000015 UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern void Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8 (void);
// 0x00000016 System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern void Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380 (void);
// 0x00000017 UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern void Rigidbody_get_angularVelocity_mA5D414D6E27755C944485A750F974BEA24CF27F0 (void);
// 0x00000018 System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern void Rigidbody_set_angularVelocity_m1839DCBC87B01EFD0B4936E84E503E38774B962C (void);
// 0x00000019 System.Single UnityEngine.Rigidbody::get_drag()
extern void Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E (void);
// 0x0000001A System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern void Rigidbody_set_drag_mCE564F278586FB0693B2BBEC4FB72E1F8E1E97EE (void);
// 0x0000001B System.Single UnityEngine.Rigidbody::get_angularDrag()
extern void Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B (void);
// 0x0000001C System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern void Rigidbody_set_angularDrag_mF70C1926190A2010B926B15E8628EA4D853ACFD9 (void);
// 0x0000001D System.Single UnityEngine.Rigidbody::get_mass()
extern void Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1 (void);
// 0x0000001E System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern void Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9 (void);
// 0x0000001F System.Boolean UnityEngine.Rigidbody::get_useGravity()
extern void Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA (void);
// 0x00000020 System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern void Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA (void);
// 0x00000021 System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern void Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2 (void);
// 0x00000022 System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern void Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B (void);
// 0x00000023 System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern void Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13 (void);
// 0x00000024 UnityEngine.RigidbodyConstraints UnityEngine.Rigidbody::get_constraints()
extern void Rigidbody_get_constraints_mC644C1F579B4B475BF174953564217123411EE64 (void);
// 0x00000025 System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern void Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F (void);
// 0x00000026 UnityEngine.CollisionDetectionMode UnityEngine.Rigidbody::get_collisionDetectionMode()
extern void Rigidbody_get_collisionDetectionMode_mFA990973D3E1F33D73D8A8FF9F68DBF7FC070A34 (void);
// 0x00000027 System.Void UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)
extern void Rigidbody_set_collisionDetectionMode_m597F293BC9DF07EA920DD19195B791FC417EA63F (void);
// 0x00000028 System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
extern void Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F (void);
// 0x00000029 UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern void Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113 (void);
// 0x0000002A System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern void Rigidbody_set_position_m54EED7F2D5EC9D34937D94B671BD6DE356DD0E7F (void);
// 0x0000002B UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern void Rigidbody_get_rotation_mD967DD98F16F80C0D74F8F1C25953D0609906BE5 (void);
// 0x0000002C System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern void Rigidbody_set_rotation_mFC6AD10748F2A0E04B6D2DBADEC168D60F90345B (void);
// 0x0000002D UnityEngine.RigidbodyInterpolation UnityEngine.Rigidbody::get_interpolation()
extern void Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95 (void);
// 0x0000002E System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
extern void Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540 (void);
// 0x0000002F System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
extern void Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142 (void);
// 0x00000030 System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern void Rigidbody_MovePosition_m5807AA5CDEC1B8350618166B2DF56FCAAAFFF7C1 (void);
// 0x00000031 System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern void Rigidbody_MoveRotation_mCBE2CF9F1B4A86C4BCB899AAB4C4EE8BBAD21C84 (void);
// 0x00000032 System.Void UnityEngine.Rigidbody::WakeUp()
extern void Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336 (void);
// 0x00000033 UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
extern void Rigidbody_GetPointVelocity_mFA72C6F1FF5AEC64B25B0F7F7682BB9666D28AC4 (void);
// 0x00000034 System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54 (void);
// 0x00000035 System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern void Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38 (void);
// 0x00000036 System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeForce_m90667A3C15C991BBF522053C5A87CBDA3BAECBB3 (void);
// 0x00000037 System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3)
extern void Rigidbody_AddRelativeForce_m438C5E9883134D09E23FA50D7438FE2C8D81510B (void);
// 0x00000038 System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60 (void);
// 0x00000039 System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern void Rigidbody_AddTorque_mB9B6AE5C84CCB626CDDAAA5307F31BC0D9589EE0 (void);
// 0x0000003A System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769 (void);
// 0x0000003B System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A (void);
// 0x0000003C System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single)
extern void Rigidbody_AddExplosionForce_m1FCC884218DB2A0DC0899C6FE4F0B131341C1243 (void);
// 0x0000003D System.Void UnityEngine.Rigidbody::.ctor()
extern void Rigidbody__ctor_m7E9FF7B44B8A3B05217339C2C852A4BF620A12E4 (void);
// 0x0000003E System.Void UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC (void);
// 0x0000003F System.Void UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D (void);
// 0x00000040 System.Void UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1 (void);
// 0x00000041 System.Void UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783 (void);
// 0x00000042 System.Void UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019 (void);
// 0x00000043 System.Void UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581 (void);
// 0x00000044 System.Void UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91 (void);
// 0x00000045 System.Void UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3 (void);
// 0x00000046 System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
extern void Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440 (void);
// 0x00000047 System.Void UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0 (void);
// 0x00000048 System.Void UnityEngine.Rigidbody::GetPointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E (void);
// 0x00000049 System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119 (void);
// 0x0000004A System.Void UnityEngine.Rigidbody::AddRelativeForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeForce_Injected_m1D129AFF4E7F635163F8453D167F16C10D53510E (void);
// 0x0000004B System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B (void);
// 0x0000004C System.Void UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F (void);
// 0x0000004D System.Void UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07 (void);
// 0x0000004E System.Boolean UnityEngine.Collider::get_enabled()
extern void Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB (void);
// 0x0000004F System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern void Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B (void);
// 0x00000050 UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern void Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8 (void);
// 0x00000051 System.Boolean UnityEngine.Collider::get_isTrigger()
extern void Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E (void);
// 0x00000052 System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
extern void Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5 (void);
// 0x00000053 UnityEngine.Vector3 UnityEngine.Collider::ClosestPoint(UnityEngine.Vector3)
extern void Collider_ClosestPoint_mA3CF53B6EE9CEEDB3BF2BCCE19E511CA659672B7 (void);
// 0x00000054 UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern void Collider_get_bounds_mD3CB68E38FB998406193A88D18C01F510272058A (void);
// 0x00000055 System.Void UnityEngine.Collider::.ctor()
extern void Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED (void);
// 0x00000056 System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Collider_ClosestPoint_Injected_mBB4217A183FB329B8F3B1DE8837CA5B12A3B7D9F (void);
// 0x00000057 System.Void UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)
extern void Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6 (void);
// 0x00000058 UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern void CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434 (void);
// 0x00000059 UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
extern void CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551 (void);
// 0x0000005A UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh()
extern void MeshCollider_get_sharedMesh_mD66AB7910B58EBCB574232E54E6A496B81A56C67 (void);
// 0x0000005B System.Boolean UnityEngine.MeshCollider::get_convex()
extern void MeshCollider_get_convex_mAA9801A31A512288CE0705E56596D836FC73E64A (void);
// 0x0000005C System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
extern void MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0 (void);
// 0x0000005D UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern void CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE (void);
// 0x0000005E System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern void CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6 (void);
// 0x0000005F System.Single UnityEngine.CapsuleCollider::get_radius()
extern void CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D (void);
// 0x00000060 System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
extern void CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D (void);
// 0x00000061 System.Single UnityEngine.CapsuleCollider::get_height()
extern void CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75 (void);
// 0x00000062 System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern void CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6 (void);
// 0x00000063 System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2 (void);
// 0x00000064 System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3 (void);
// 0x00000065 UnityEngine.Vector3 UnityEngine.BoxCollider::get_center()
extern void BoxCollider_get_center_mA9164B9949F419A35CC949685F1DC14588BC6402 (void);
// 0x00000066 System.Void UnityEngine.BoxCollider::set_center(UnityEngine.Vector3)
extern void BoxCollider_set_center_m8A871056CA383C9932A7694FE396A1EFA247FC69 (void);
// 0x00000067 UnityEngine.Vector3 UnityEngine.BoxCollider::get_size()
extern void BoxCollider_get_size_m1C7DA815D3BA9DDB3D92A58BEEFE2FCBA5206FE2 (void);
// 0x00000068 System.Void UnityEngine.BoxCollider::set_size(UnityEngine.Vector3)
extern void BoxCollider_set_size_m65F9B4BD610D3094313EC8D1C5CE58D1D345A176 (void);
// 0x00000069 System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8 (void);
// 0x0000006A System.Void UnityEngine.BoxCollider::set_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4 (void);
// 0x0000006B System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1 (void);
// 0x0000006C System.Void UnityEngine.BoxCollider::set_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943 (void);
// 0x0000006D UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern void SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00 (void);
// 0x0000006E System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern void SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE (void);
// 0x0000006F System.Single UnityEngine.SphereCollider::get_radius()
extern void SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094 (void);
// 0x00000070 System.Void UnityEngine.SphereCollider::set_radius(System.Single)
extern void SphereCollider_set_radius_m3161573A2D89F495F4B79E16C52B905C0F9AD699 (void);
// 0x00000071 System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB (void);
// 0x00000072 System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C (void);
// 0x00000073 System.Void UnityEngine.ConstantForce::set_force(UnityEngine.Vector3)
extern void ConstantForce_set_force_mE1A43FDFDE40D0ED481E1FFCE1A0DEC5331A8726 (void);
// 0x00000074 System.Void UnityEngine.ConstantForce::set_relativeForce(UnityEngine.Vector3)
extern void ConstantForce_set_relativeForce_mA40EA710E94640211EE4226805EDE789CDB04FC8 (void);
// 0x00000075 System.Void UnityEngine.ConstantForce::set_force_Injected(UnityEngine.Vector3&)
extern void ConstantForce_set_force_Injected_m0ACDE6E2312881351569FE2667AD81D03E9FFB02 (void);
// 0x00000076 System.Void UnityEngine.ConstantForce::set_relativeForce_Injected(UnityEngine.Vector3&)
extern void ConstantForce_set_relativeForce_Injected_m813A6CF88D8711330D1010172CCCC7F881C94B63 (void);
// 0x00000077 UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
extern void Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73 (void);
// 0x00000078 System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
extern void Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762 (void);
// 0x00000079 UnityEngine.Vector3 UnityEngine.Joint::get_axis()
extern void Joint_get_axis_m54A1BFEC3A3594CFFCF0C21FBEC15B5ADA7CB702 (void);
// 0x0000007A System.Void UnityEngine.Joint::set_axis(UnityEngine.Vector3)
extern void Joint_set_axis_mD77BFEA4D264E9365979261520BA5B83D86E26BA (void);
// 0x0000007B UnityEngine.Vector3 UnityEngine.Joint::get_anchor()
extern void Joint_get_anchor_m50C893690CD5C0E85C9A7F9AB66685BB990EF662 (void);
// 0x0000007C System.Void UnityEngine.Joint::set_anchor(UnityEngine.Vector3)
extern void Joint_set_anchor_m9584AAAB1BA704C95E4C5C2E4844E45E3ADCA9D3 (void);
// 0x0000007D UnityEngine.Vector3 UnityEngine.Joint::get_connectedAnchor()
extern void Joint_get_connectedAnchor_mA5051D7B6BE0F109E3AE454E05CDE706FCB39D46 (void);
// 0x0000007E System.Single UnityEngine.Joint::get_breakForce()
extern void Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681 (void);
// 0x0000007F System.Void UnityEngine.Joint::set_breakForce(System.Single)
extern void Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7 (void);
// 0x00000080 System.Boolean UnityEngine.Joint::get_enableCollision()
extern void Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4 (void);
// 0x00000081 System.Void UnityEngine.Joint::set_enableCollision(System.Boolean)
extern void Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB (void);
// 0x00000082 System.Void UnityEngine.Joint::get_axis_Injected(UnityEngine.Vector3&)
extern void Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02 (void);
// 0x00000083 System.Void UnityEngine.Joint::set_axis_Injected(UnityEngine.Vector3&)
extern void Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4 (void);
// 0x00000084 System.Void UnityEngine.Joint::get_anchor_Injected(UnityEngine.Vector3&)
extern void Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B (void);
// 0x00000085 System.Void UnityEngine.Joint::set_anchor_Injected(UnityEngine.Vector3&)
extern void Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB (void);
// 0x00000086 System.Void UnityEngine.Joint::get_connectedAnchor_Injected(UnityEngine.Vector3&)
extern void Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C (void);
// 0x00000087 UnityEngine.JointLimits UnityEngine.HingeJoint::get_limits()
extern void HingeJoint_get_limits_m988B1E8056689C22248ABB05272559518C3E6990 (void);
// 0x00000088 System.Void UnityEngine.HingeJoint::set_limits(UnityEngine.JointLimits)
extern void HingeJoint_set_limits_m80EAEE715B33AC3760C98C71A3D19CA73073C4F2 (void);
// 0x00000089 UnityEngine.JointSpring UnityEngine.HingeJoint::get_spring()
extern void HingeJoint_get_spring_m0BFA4E9ED60DBBDC97F43B4FF44C1F7E59E99789 (void);
// 0x0000008A System.Void UnityEngine.HingeJoint::set_spring(UnityEngine.JointSpring)
extern void HingeJoint_set_spring_m47D05FDD4FFFE6DDFCFC5B44C04D7CAF08EB40B5 (void);
// 0x0000008B System.Void UnityEngine.HingeJoint::set_useLimits(System.Boolean)
extern void HingeJoint_set_useLimits_mBE2545A0DC5E628DF0D308DDEA844544DDD48711 (void);
// 0x0000008C System.Boolean UnityEngine.HingeJoint::get_useSpring()
extern void HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776 (void);
// 0x0000008D System.Void UnityEngine.HingeJoint::set_useSpring(System.Boolean)
extern void HingeJoint_set_useSpring_mF7D87E80B7684FCA70EE1D7237854506AFDBCD90 (void);
// 0x0000008E System.Single UnityEngine.HingeJoint::get_angle()
extern void HingeJoint_get_angle_mF6EDA90ABF2375876483BDE4C41D854723E6A7F8 (void);
// 0x0000008F System.Void UnityEngine.HingeJoint::get_limits_Injected(UnityEngine.JointLimits&)
extern void HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161 (void);
// 0x00000090 System.Void UnityEngine.HingeJoint::set_limits_Injected(UnityEngine.JointLimits&)
extern void HingeJoint_set_limits_Injected_m7BE08D8623D79E370C672A0619C8070E33CDCA6D (void);
// 0x00000091 System.Void UnityEngine.HingeJoint::get_spring_Injected(UnityEngine.JointSpring&)
extern void HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A (void);
// 0x00000092 System.Void UnityEngine.HingeJoint::set_spring_Injected(UnityEngine.JointSpring&)
extern void HingeJoint_set_spring_Injected_m1DD3766A6E83E88B1119CD76890F3A4A259F7B13 (void);
// 0x00000093 System.Void UnityEngine.SpringJoint::set_spring(System.Single)
extern void SpringJoint_set_spring_mC99EBC17935135ECBCB7AFEA61EEA1BFC38A5166 (void);
// 0x00000094 System.Void UnityEngine.SpringJoint::set_damper(System.Single)
extern void SpringJoint_set_damper_mED3715ED4A65B15EA526AFB39C4A4B9F1126E0BF (void);
// 0x00000095 System.Void UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310 (void);
// 0x00000096 System.Void UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5 (void);
// 0x00000097 System.Void UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A (void);
// 0x00000098 System.Void UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E (void);
// 0x00000099 System.Void UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360 (void);
// 0x0000009A System.Void UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE (void);
// 0x0000009B UnityEngine.SoftJointLimit UnityEngine.ConfigurableJoint::get_linearLimit()
extern void ConfigurableJoint_get_linearLimit_mD5F75C83411FD2253551B9136A774467203FB725 (void);
// 0x0000009C System.Void UnityEngine.ConfigurableJoint::set_linearLimit(UnityEngine.SoftJointLimit)
extern void ConfigurableJoint_set_linearLimit_mF600A726E8D4E5A49DABE0756636AD77FF7F9406 (void);
// 0x0000009D UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_targetPosition()
extern void ConfigurableJoint_get_targetPosition_mC5616960F574B43D4138CC8EFB93B09582465F51 (void);
// 0x0000009E System.Void UnityEngine.ConfigurableJoint::set_targetPosition(UnityEngine.Vector3)
extern void ConfigurableJoint_set_targetPosition_m50CA64078EC972DC67656945DF2AC66B799A391E (void);
// 0x0000009F System.Void UnityEngine.ConfigurableJoint::set_targetVelocity(UnityEngine.Vector3)
extern void ConfigurableJoint_set_targetVelocity_mC7FC3152E8B8A4B8BC5FB6038862043AC95DDB90 (void);
// 0x000000A0 System.Void UnityEngine.ConfigurableJoint::set_xDrive(UnityEngine.JointDrive)
extern void ConfigurableJoint_set_xDrive_m2D7CACFF7CF970126AD18F4DEE9BA718B2F4DDBF (void);
// 0x000000A1 System.Void UnityEngine.ConfigurableJoint::set_yDrive(UnityEngine.JointDrive)
extern void ConfigurableJoint_set_yDrive_m3FB05F7B4F391074AF6978EBC893EA256E53C8DB (void);
// 0x000000A2 System.Void UnityEngine.ConfigurableJoint::set_zDrive(UnityEngine.JointDrive)
extern void ConfigurableJoint_set_zDrive_mBDA2837C2BF86C092D7DE883D66A19567D40A76C (void);
// 0x000000A3 System.Void UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)
extern void ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E (void);
// 0x000000A4 System.Void UnityEngine.ConfigurableJoint::get_linearLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_get_linearLimit_Injected_m6D71CA41EDB3C2505A46BE0DB0CC99A251A7F5E0 (void);
// 0x000000A5 System.Void UnityEngine.ConfigurableJoint::set_linearLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236 (void);
// 0x000000A6 System.Void UnityEngine.ConfigurableJoint::get_targetPosition_Injected(UnityEngine.Vector3&)
extern void ConfigurableJoint_get_targetPosition_Injected_mDEC7643773FB5CC2C595A3D4BAAAC16CFB6CFAB6 (void);
// 0x000000A7 System.Void UnityEngine.ConfigurableJoint::set_targetPosition_Injected(UnityEngine.Vector3&)
extern void ConfigurableJoint_set_targetPosition_Injected_m6A0202B69C0EBE90A965CD35D08687BC92BA4F62 (void);
// 0x000000A8 System.Void UnityEngine.ConfigurableJoint::set_targetVelocity_Injected(UnityEngine.Vector3&)
extern void ConfigurableJoint_set_targetVelocity_Injected_mFEF54FBAD7A7047851F6AEC9B2443D716DDB1611 (void);
// 0x000000A9 System.Void UnityEngine.ConfigurableJoint::set_xDrive_Injected(UnityEngine.JointDrive&)
extern void ConfigurableJoint_set_xDrive_Injected_m156BE81B424415D018AD4F9EDA557176B4EBE550 (void);
// 0x000000AA System.Void UnityEngine.ConfigurableJoint::set_yDrive_Injected(UnityEngine.JointDrive&)
extern void ConfigurableJoint_set_yDrive_Injected_mEA2F31645634EDD6415C1613EC0D536AEE0708FD (void);
// 0x000000AB System.Void UnityEngine.ConfigurableJoint::set_zDrive_Injected(UnityEngine.JointDrive&)
extern void ConfigurableJoint_set_zDrive_Injected_m120527F4C2C505DA741AE82B0D9F5819457AD916 (void);
// 0x000000AC UnityEngine.Collider UnityEngine.ContactPoint::get_thisCollider()
extern void ContactPoint_get_thisCollider_mD61A196BDF1A64DA4DEE4FEB69BEFB7D103D08C3 (void);
// 0x000000AD UnityEngine.Collider UnityEngine.ContactPoint::GetColliderByInstanceID(System.Int32)
extern void ContactPoint_GetColliderByInstanceID_mB54CD46A49B86BCA018696696B9D70795BDA5F73 (void);
// 0x000000AE System.String UnityEngine.PhysicsScene::ToString()
extern void PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9 (void);
// 0x000000AF System.Int32 UnityEngine.PhysicsScene::GetHashCode()
extern void PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654 (void);
// 0x000000B0 System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
extern void PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9 (void);
// 0x000000B1 System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
extern void PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8 (void);
// 0x000000B2 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336 (void);
// 0x000000B3 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F (void);
// 0x000000B4 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B (void);
// 0x000000B5 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529 (void);
// 0x000000B6 System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1 (void);
// 0x000000B7 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1 (void);
// 0x000000B8 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_CapsuleCast_m62813C3DE93000767BDF5E789E7D82B6944B96E9 (void);
// 0x000000B9 System.Boolean UnityEngine.PhysicsScene::Internal_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_CapsuleCast_m98099F6A0153C33964D1DC312467B438F2CD02BB (void);
// 0x000000BA System.Boolean UnityEngine.PhysicsScene::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_CapsuleCast_mBA2BB56A5EFA893BE437056DBBE6B91D9326B0BC (void);
// 0x000000BB System.Boolean UnityEngine.PhysicsScene::Query_BoxCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_BoxCast_m53E52DFB03C8C702A5F5B591F4DA3E46CF2B923C (void);
// 0x000000BC System.Boolean UnityEngine.PhysicsScene::Internal_BoxCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_BoxCast_mD2D894CADDE505795A75473F4B88ED330A23C2D4 (void);
// 0x000000BD System.Boolean UnityEngine.PhysicsScene::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,UnityEngine.Quaternion,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_BoxCast_m67054123A6D81CA9CD60888988AF034D1171CFBA (void);
// 0x000000BE System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF (void);
// 0x000000BF System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA (void);
// 0x000000C0 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E (void);
// 0x000000C1 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_CapsuleCast_Injected_mB12FAFD383D49067D93BFCADF6EBB21FDF89711B (void);
// 0x000000C2 System.Boolean UnityEngine.PhysicsScene::Query_BoxCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_BoxCast_Injected_mDCCFFFC0A8690F4CD9EB4156A0577F67AF6F3747 (void);
// 0x000000C3 UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
extern void Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30 (void);
// 0x000000C4 System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
extern void Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B (void);
// 0x000000C5 System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider)
extern void Physics_IgnoreCollision_m2F9164DBBB3036E669985F1DB478502199FA9B70 (void);
// 0x000000C6 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C (void);
// 0x000000C7 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA (void);
// 0x000000C8 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D (void);
// 0x000000C9 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E (void);
// 0x000000CA System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1 (void);
// 0x000000CB System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024 (void);
// 0x000000CC System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B (void);
// 0x000000CD System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287 (void);
// 0x000000CE System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47 (void);
// 0x000000CF System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1 (void);
// 0x000000D0 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern void Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5 (void);
// 0x000000D1 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern void Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A (void);
// 0x000000D2 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8 (void);
// 0x000000D3 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950 (void);
// 0x000000D4 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67 (void);
// 0x000000D5 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern void Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495 (void);
// 0x000000D6 System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6 (void);
// 0x000000D7 System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CapsuleCast_mE59C9FD33569870B9C811E5930CB5D0D7A43DADC (void);
// 0x000000D8 System.Boolean UnityEngine.Physics::BoxCast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,UnityEngine.Quaternion,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_BoxCast_m58F80F9BDF3A6FF09B4FB7702A22270F5849677E (void);
// 0x000000D9 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851 (void);
// 0x000000DA UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D (void);
// 0x000000DB UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC (void);
// 0x000000DC UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04 (void);
// 0x000000DD UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE (void);
// 0x000000DE UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D (void);
// 0x000000DF UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4 (void);
// 0x000000E0 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern void Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F (void);
// 0x000000E1 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern void Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056 (void);
// 0x000000E2 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE (void);
// 0x000000E3 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894 (void);
// 0x000000E4 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9 (void);
// 0x000000E5 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F (void);
// 0x000000E6 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F (void);
// 0x000000E7 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB (void);
// 0x000000E8 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316 (void);
// 0x000000E9 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88 (void);
// 0x000000EA UnityEngine.RaycastHit[] UnityEngine.Physics::Query_CapsuleCastAll(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Query_CapsuleCastAll_m1792D802EB297607DE26AAF618B09429B357C795 (void);
// 0x000000EB UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CapsuleCastAll_mF02568415D812E849250B3336D61EE42CEFD9BC5 (void);
// 0x000000EC UnityEngine.RaycastHit[] UnityEngine.Physics::CapsuleCastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single)
extern void Physics_CapsuleCastAll_m53D5120E2317219D62515F31CBC16260C5868BDB (void);
// 0x000000ED System.Boolean UnityEngine.Physics::CheckSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_Internal_m18DE2D992B23586FE71156658B220D68A9F87A4C (void);
// 0x000000EE System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_m09DEF32BABFD5ABF7BBDA4E6A9FB82256C90E54F (void);
// 0x000000EF System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_CheckSphere_mC209BB79CC200ED33DAD3EA9E0FAF2C1D6DA4385 (void);
// 0x000000F0 System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
extern void Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262 (void);
// 0x000000F1 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913 (void);
// 0x000000F2 UnityEngine.RaycastHit[] UnityEngine.Physics::Query_CapsuleCastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Query_CapsuleCastAll_Injected_m7362198260F0EC4DFBBA0525CBFE72D89A582CF2 (void);
// 0x000000F3 System.Boolean UnityEngine.Physics::CheckSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_Internal_Injected_m1A14F396FC5B7267169141CEFB8466B5364B0866 (void);
static Il2CppMethodPointer s_methodPointers[243] = 
{
	SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03,
	SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26,
	JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8,
	JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF,
	JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945,
	JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001,
	JointLimits_set_min_mBEF6F81D32295EE572F03C59CC28F13ECA322DE4,
	JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F,
	JointLimits_set_max_m71FC88C4B4720B81128A9906CD5E1D018D207FB8,
	JointLimits_set_bounciness_mD9CF3456A5E3E46FD36481A6E3D76F91505D12DC,
	Collision_get_relativeVelocity_m0FB1600821CDCD3C52D56F80304A509FC1DF702E,
	Collision_get_collider_m52F32CFE0BC1925C72A5B8EB743BBEF628201352,
	Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49,
	Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF,
	RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9,
	RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001,
	RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94,
	RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543,
	RaycastHit_get_transform_m3C0BEE7439CA37F82FD5216143B92BF32F995279,
	RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC,
	Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8,
	Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380,
	Rigidbody_get_angularVelocity_mA5D414D6E27755C944485A750F974BEA24CF27F0,
	Rigidbody_set_angularVelocity_m1839DCBC87B01EFD0B4936E84E503E38774B962C,
	Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E,
	Rigidbody_set_drag_mCE564F278586FB0693B2BBEC4FB72E1F8E1E97EE,
	Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B,
	Rigidbody_set_angularDrag_mF70C1926190A2010B926B15E8628EA4D853ACFD9,
	Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1,
	Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9,
	Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA,
	Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA,
	Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2,
	Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B,
	Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13,
	Rigidbody_get_constraints_mC644C1F579B4B475BF174953564217123411EE64,
	Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F,
	Rigidbody_get_collisionDetectionMode_mFA990973D3E1F33D73D8A8FF9F68DBF7FC070A34,
	Rigidbody_set_collisionDetectionMode_m597F293BC9DF07EA920DD19195B791FC417EA63F,
	Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F,
	Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113,
	Rigidbody_set_position_m54EED7F2D5EC9D34937D94B671BD6DE356DD0E7F,
	Rigidbody_get_rotation_mD967DD98F16F80C0D74F8F1C25953D0609906BE5,
	Rigidbody_set_rotation_mFC6AD10748F2A0E04B6D2DBADEC168D60F90345B,
	Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95,
	Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540,
	Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142,
	Rigidbody_MovePosition_m5807AA5CDEC1B8350618166B2DF56FCAAAFFF7C1,
	Rigidbody_MoveRotation_mCBE2CF9F1B4A86C4BCB899AAB4C4EE8BBAD21C84,
	Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336,
	Rigidbody_GetPointVelocity_mFA72C6F1FF5AEC64B25B0F7F7682BB9666D28AC4,
	Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54,
	Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38,
	Rigidbody_AddRelativeForce_m90667A3C15C991BBF522053C5A87CBDA3BAECBB3,
	Rigidbody_AddRelativeForce_m438C5E9883134D09E23FA50D7438FE2C8D81510B,
	Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60,
	Rigidbody_AddTorque_mB9B6AE5C84CCB626CDDAAA5307F31BC0D9589EE0,
	Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769,
	Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A,
	Rigidbody_AddExplosionForce_m1FCC884218DB2A0DC0899C6FE4F0B131341C1243,
	Rigidbody__ctor_m7E9FF7B44B8A3B05217339C2C852A4BF620A12E4,
	Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC,
	Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D,
	Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1,
	Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783,
	Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019,
	Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581,
	Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91,
	Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3,
	Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440,
	Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0,
	Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E,
	Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119,
	Rigidbody_AddRelativeForce_Injected_m1D129AFF4E7F635163F8453D167F16C10D53510E,
	Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B,
	Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F,
	Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07,
	Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB,
	Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B,
	Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8,
	Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E,
	Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5,
	Collider_ClosestPoint_mA3CF53B6EE9CEEDB3BF2BCCE19E511CA659672B7,
	Collider_get_bounds_mD3CB68E38FB998406193A88D18C01F510272058A,
	Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED,
	Collider_ClosestPoint_Injected_mBB4217A183FB329B8F3B1DE8837CA5B12A3B7D9F,
	Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6,
	CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434,
	CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551,
	MeshCollider_get_sharedMesh_mD66AB7910B58EBCB574232E54E6A496B81A56C67,
	MeshCollider_get_convex_mAA9801A31A512288CE0705E56596D836FC73E64A,
	MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0,
	CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE,
	CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6,
	CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D,
	CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D,
	CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75,
	CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6,
	CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2,
	CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3,
	BoxCollider_get_center_mA9164B9949F419A35CC949685F1DC14588BC6402,
	BoxCollider_set_center_m8A871056CA383C9932A7694FE396A1EFA247FC69,
	BoxCollider_get_size_m1C7DA815D3BA9DDB3D92A58BEEFE2FCBA5206FE2,
	BoxCollider_set_size_m65F9B4BD610D3094313EC8D1C5CE58D1D345A176,
	BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8,
	BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4,
	BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1,
	BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943,
	SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00,
	SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE,
	SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094,
	SphereCollider_set_radius_m3161573A2D89F495F4B79E16C52B905C0F9AD699,
	SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB,
	SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C,
	ConstantForce_set_force_mE1A43FDFDE40D0ED481E1FFCE1A0DEC5331A8726,
	ConstantForce_set_relativeForce_mA40EA710E94640211EE4226805EDE789CDB04FC8,
	ConstantForce_set_force_Injected_m0ACDE6E2312881351569FE2667AD81D03E9FFB02,
	ConstantForce_set_relativeForce_Injected_m813A6CF88D8711330D1010172CCCC7F881C94B63,
	Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73,
	Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762,
	Joint_get_axis_m54A1BFEC3A3594CFFCF0C21FBEC15B5ADA7CB702,
	Joint_set_axis_mD77BFEA4D264E9365979261520BA5B83D86E26BA,
	Joint_get_anchor_m50C893690CD5C0E85C9A7F9AB66685BB990EF662,
	Joint_set_anchor_m9584AAAB1BA704C95E4C5C2E4844E45E3ADCA9D3,
	Joint_get_connectedAnchor_mA5051D7B6BE0F109E3AE454E05CDE706FCB39D46,
	Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681,
	Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7,
	Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4,
	Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB,
	Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02,
	Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4,
	Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B,
	Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB,
	Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C,
	HingeJoint_get_limits_m988B1E8056689C22248ABB05272559518C3E6990,
	HingeJoint_set_limits_m80EAEE715B33AC3760C98C71A3D19CA73073C4F2,
	HingeJoint_get_spring_m0BFA4E9ED60DBBDC97F43B4FF44C1F7E59E99789,
	HingeJoint_set_spring_m47D05FDD4FFFE6DDFCFC5B44C04D7CAF08EB40B5,
	HingeJoint_set_useLimits_mBE2545A0DC5E628DF0D308DDEA844544DDD48711,
	HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776,
	HingeJoint_set_useSpring_mF7D87E80B7684FCA70EE1D7237854506AFDBCD90,
	HingeJoint_get_angle_mF6EDA90ABF2375876483BDE4C41D854723E6A7F8,
	HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161,
	HingeJoint_set_limits_Injected_m7BE08D8623D79E370C672A0619C8070E33CDCA6D,
	HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A,
	HingeJoint_set_spring_Injected_m1DD3766A6E83E88B1119CD76890F3A4A259F7B13,
	SpringJoint_set_spring_mC99EBC17935135ECBCB7AFEA61EEA1BFC38A5166,
	SpringJoint_set_damper_mED3715ED4A65B15EA526AFB39C4A4B9F1126E0BF,
	ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310,
	ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5,
	ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A,
	ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E,
	ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360,
	ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE,
	ConfigurableJoint_get_linearLimit_mD5F75C83411FD2253551B9136A774467203FB725,
	ConfigurableJoint_set_linearLimit_mF600A726E8D4E5A49DABE0756636AD77FF7F9406,
	ConfigurableJoint_get_targetPosition_mC5616960F574B43D4138CC8EFB93B09582465F51,
	ConfigurableJoint_set_targetPosition_m50CA64078EC972DC67656945DF2AC66B799A391E,
	ConfigurableJoint_set_targetVelocity_mC7FC3152E8B8A4B8BC5FB6038862043AC95DDB90,
	ConfigurableJoint_set_xDrive_m2D7CACFF7CF970126AD18F4DEE9BA718B2F4DDBF,
	ConfigurableJoint_set_yDrive_m3FB05F7B4F391074AF6978EBC893EA256E53C8DB,
	ConfigurableJoint_set_zDrive_mBDA2837C2BF86C092D7DE883D66A19567D40A76C,
	ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E,
	ConfigurableJoint_get_linearLimit_Injected_m6D71CA41EDB3C2505A46BE0DB0CC99A251A7F5E0,
	ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236,
	ConfigurableJoint_get_targetPosition_Injected_mDEC7643773FB5CC2C595A3D4BAAAC16CFB6CFAB6,
	ConfigurableJoint_set_targetPosition_Injected_m6A0202B69C0EBE90A965CD35D08687BC92BA4F62,
	ConfigurableJoint_set_targetVelocity_Injected_mFEF54FBAD7A7047851F6AEC9B2443D716DDB1611,
	ConfigurableJoint_set_xDrive_Injected_m156BE81B424415D018AD4F9EDA557176B4EBE550,
	ConfigurableJoint_set_yDrive_Injected_mEA2F31645634EDD6415C1613EC0D536AEE0708FD,
	ConfigurableJoint_set_zDrive_Injected_m120527F4C2C505DA741AE82B0D9F5819457AD916,
	ContactPoint_get_thisCollider_mD61A196BDF1A64DA4DEE4FEB69BEFB7D103D08C3,
	ContactPoint_GetColliderByInstanceID_mB54CD46A49B86BCA018696696B9D70795BDA5F73,
	PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9,
	PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654,
	PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9,
	PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8,
	PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336,
	PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F,
	PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B,
	PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529,
	PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1,
	PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1,
	PhysicsScene_Query_CapsuleCast_m62813C3DE93000767BDF5E789E7D82B6944B96E9,
	PhysicsScene_Internal_CapsuleCast_m98099F6A0153C33964D1DC312467B438F2CD02BB,
	PhysicsScene_CapsuleCast_mBA2BB56A5EFA893BE437056DBBE6B91D9326B0BC,
	PhysicsScene_Query_BoxCast_m53E52DFB03C8C702A5F5B591F4DA3E46CF2B923C,
	PhysicsScene_Internal_BoxCast_mD2D894CADDE505795A75473F4B88ED330A23C2D4,
	PhysicsScene_BoxCast_m67054123A6D81CA9CD60888988AF034D1171CFBA,
	PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF,
	PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA,
	PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E,
	PhysicsScene_Query_CapsuleCast_Injected_mB12FAFD383D49067D93BFCADF6EBB21FDF89711B,
	PhysicsScene_Query_BoxCast_Injected_mDCCFFFC0A8690F4CD9EB4156A0577F67AF6F3747,
	Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30,
	Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B,
	Physics_IgnoreCollision_m2F9164DBBB3036E669985F1DB478502199FA9B70,
	Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C,
	Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA,
	Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D,
	Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E,
	Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1,
	Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024,
	Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B,
	Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287,
	Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47,
	Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1,
	Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5,
	Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A,
	Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8,
	Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950,
	Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67,
	Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495,
	Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6,
	Physics_CapsuleCast_mE59C9FD33569870B9C811E5930CB5D0D7A43DADC,
	Physics_BoxCast_m58F80F9BDF3A6FF09B4FB7702A22270F5849677E,
	Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851,
	Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D,
	Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC,
	Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04,
	Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE,
	Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D,
	Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4,
	Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F,
	Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056,
	Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE,
	Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894,
	Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9,
	Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F,
	Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F,
	Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB,
	Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316,
	Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88,
	Physics_Query_CapsuleCastAll_m1792D802EB297607DE26AAF618B09429B357C795,
	Physics_CapsuleCastAll_mF02568415D812E849250B3336D61EE42CEFD9BC5,
	Physics_CapsuleCastAll_m53D5120E2317219D62515F31CBC16260C5868BDB,
	Physics_CheckSphere_Internal_m18DE2D992B23586FE71156658B220D68A9F87A4C,
	Physics_CheckSphere_m09DEF32BABFD5ABF7BBDA4E6A9FB82256C90E54F,
	Physics_CheckSphere_mC209BB79CC200ED33DAD3EA9E0FAF2C1D6DA4385,
	Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262,
	Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913,
	Physics_Query_CapsuleCastAll_Injected_m7362198260F0EC4DFBBA0525CBFE72D89A582CF2,
	Physics_CheckSphere_Internal_Injected_m1A14F396FC5B7267169141CEFB8466B5364B0866,
};
extern void SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03_AdjustorThunk (void);
extern void SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26_AdjustorThunk (void);
extern void JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8_AdjustorThunk (void);
extern void JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF_AdjustorThunk (void);
extern void JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945_AdjustorThunk (void);
extern void JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001_AdjustorThunk (void);
extern void JointLimits_set_min_mBEF6F81D32295EE572F03C59CC28F13ECA322DE4_AdjustorThunk (void);
extern void JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F_AdjustorThunk (void);
extern void JointLimits_set_max_m71FC88C4B4720B81128A9906CD5E1D018D207FB8_AdjustorThunk (void);
extern void JointLimits_set_bounciness_mD9CF3456A5E3E46FD36481A6E3D76F91505D12DC_AdjustorThunk (void);
extern void RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk (void);
extern void RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk (void);
extern void RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk (void);
extern void RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk (void);
extern void RaycastHit_get_transform_m3C0BEE7439CA37F82FD5216143B92BF32F995279_AdjustorThunk (void);
extern void RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_AdjustorThunk (void);
extern void ContactPoint_get_thisCollider_mD61A196BDF1A64DA4DEE4FEB69BEFB7D103D08C3_AdjustorThunk (void);
extern void PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk (void);
extern void PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk (void);
extern void PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk (void);
extern void PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk (void);
extern void PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk (void);
extern void PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk (void);
extern void PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk (void);
extern void PhysicsScene_CapsuleCast_mBA2BB56A5EFA893BE437056DBBE6B91D9326B0BC_AdjustorThunk (void);
extern void PhysicsScene_BoxCast_m67054123A6D81CA9CD60888988AF034D1171CFBA_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[26] = 
{
	{ 0x06000001, SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03_AdjustorThunk },
	{ 0x06000002, SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26_AdjustorThunk },
	{ 0x06000003, JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8_AdjustorThunk },
	{ 0x06000004, JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF_AdjustorThunk },
	{ 0x06000005, JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945_AdjustorThunk },
	{ 0x06000006, JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001_AdjustorThunk },
	{ 0x06000007, JointLimits_set_min_mBEF6F81D32295EE572F03C59CC28F13ECA322DE4_AdjustorThunk },
	{ 0x06000008, JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F_AdjustorThunk },
	{ 0x06000009, JointLimits_set_max_m71FC88C4B4720B81128A9906CD5E1D018D207FB8_AdjustorThunk },
	{ 0x0600000A, JointLimits_set_bounciness_mD9CF3456A5E3E46FD36481A6E3D76F91505D12DC_AdjustorThunk },
	{ 0x0600000F, RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk },
	{ 0x06000010, RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk },
	{ 0x06000011, RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk },
	{ 0x06000012, RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk },
	{ 0x06000013, RaycastHit_get_transform_m3C0BEE7439CA37F82FD5216143B92BF32F995279_AdjustorThunk },
	{ 0x06000014, RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_AdjustorThunk },
	{ 0x060000AC, ContactPoint_get_thisCollider_mD61A196BDF1A64DA4DEE4FEB69BEFB7D103D08C3_AdjustorThunk },
	{ 0x060000AE, PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk },
	{ 0x060000AF, PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk },
	{ 0x060000B0, PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk },
	{ 0x060000B1, PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk },
	{ 0x060000B2, PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk },
	{ 0x060000B4, PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk },
	{ 0x060000B6, PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk },
	{ 0x060000BA, PhysicsScene_CapsuleCast_mBA2BB56A5EFA893BE437056DBBE6B91D9326B0BC_AdjustorThunk },
	{ 0x060000BD, PhysicsScene_BoxCast_m67054123A6D81CA9CD60888988AF034D1171CFBA_AdjustorThunk },
};
static const int32_t s_InvokerIndices[243] = 
{
	689,
	296,
	296,
	296,
	296,
	689,
	296,
	689,
	296,
	296,
	1135,
	14,
	14,
	14,
	14,
	1135,
	1135,
	689,
	14,
	14,
	1135,
	1136,
	1135,
	1136,
	689,
	296,
	689,
	296,
	689,
	296,
	114,
	31,
	114,
	31,
	31,
	10,
	32,
	10,
	32,
	31,
	1135,
	1136,
	1353,
	1354,
	10,
	32,
	296,
	1136,
	1354,
	23,
	1120,
	1355,
	1136,
	1355,
	1136,
	1355,
	1136,
	1565,
	1566,
	1567,
	23,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	530,
	64,
	64,
	64,
	1568,
	1569,
	114,
	31,
	14,
	114,
	31,
	1120,
	1183,
	23,
	530,
	6,
	1570,
	1111,
	14,
	114,
	31,
	1135,
	1136,
	689,
	296,
	689,
	296,
	6,
	6,
	1135,
	1136,
	1135,
	1136,
	6,
	6,
	6,
	6,
	1135,
	1136,
	689,
	296,
	6,
	6,
	1136,
	1136,
	6,
	6,
	14,
	26,
	1135,
	1136,
	1135,
	1136,
	1135,
	689,
	296,
	114,
	31,
	6,
	6,
	6,
	6,
	6,
	1571,
	1572,
	1573,
	1574,
	31,
	114,
	31,
	689,
	6,
	6,
	6,
	6,
	296,
	296,
	32,
	32,
	32,
	32,
	32,
	32,
	1575,
	1576,
	1135,
	1136,
	1136,
	1577,
	1577,
	1577,
	31,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	14,
	43,
	14,
	10,
	9,
	1578,
	1579,
	1580,
	1581,
	1582,
	1583,
	1584,
	1585,
	1586,
	1587,
	1588,
	1589,
	1590,
	1591,
	1592,
	1593,
	1594,
	1595,
	1596,
	782,
	134,
	1597,
	1598,
	1599,
	1279,
	1600,
	1601,
	1602,
	1603,
	1604,
	1605,
	1606,
	1607,
	1608,
	1609,
	1610,
	1611,
	1612,
	1613,
	1614,
	1615,
	1616,
	1617,
	1618,
	1619,
	1620,
	1621,
	1622,
	1623,
	1624,
	1625,
	1626,
	1627,
	1628,
	1629,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	17,
	1638,
	1639,
	1591,
};
extern const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule = 
{
	"UnityEngine.PhysicsModule.dll",
	243,
	s_methodPointers,
	26,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
